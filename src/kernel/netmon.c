#include <linux/module.h>
#include <linux/kernel.h>  /* Needed for KERN_ALERT */
#include <linux/version.h>  /* Needed for KERN_ALERT */

/* Networking includes */
#include <linux/if_ether.h>
#include <linux/etherdevice.h>

#include <linux/types.h>

/* Threading and scheduling includes */
#include <linux/kthread.h>

/* CPU Stats includes */
#include <linux/cpumask.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/kernel_stat.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/seq_file.h>
#include <linux/slab.h>
#include <linux/time.h>
#include <linux/irqnr.h>
#include <asm/cputime.h>
#include <linux/tick.h>

#ifndef arch_irq_stat_cpu
#define arch_irq_stat_cpu(cpu) 0
#endif
#ifndef arch_irq_stat
#define arch_irq_stat() 0
#endif
#ifndef arch_idle_time
#define arch_idle_time(cpu) 0
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,5,0)
#define cputime64_zero (0ULL)
#endif

#include "netmon.h"
#include "net_util.h"
#include "/opt/openvswitch-git/datapath/flow.h"

static char *nmp_iface  = "eth0";
module_param(nmp_iface, charp, 0764);
MODULE_PARM_DESC(nmp_iface, "Interface to report netmon");

static int nmp_report_id  = 1;
module_param(nmp_report_id, int, 0764);
MODULE_PARM_DESC(nmp_report_id, "My id");

static int nmp_net_interval = HZ;
module_param(nmp_net_interval, int, 0764);
MODULE_PARM_DESC(nmp_net_interval,
	"Time interval between flow reports (in jiffies)");

static int nmp_cpu_interval = 1000;
module_param(nmp_cpu_interval, int, 0764);
MODULE_PARM_DESC(nmp_cpu_interval, 
	"Time interval between cpu reports (in ms)");

static int nmp_flow_limit = 0;
module_param(nmp_flow_limit, int, 0764);
MODULE_PARM_DESC(nmp_flow_limit, "Threshold (in bytes) to generate a report");

static char *nmp_server_mac = "00:00:00:00:00:00";
module_param(nmp_server_mac, charp, 0764);
MODULE_PARM_DESC(nmp_server_mac, "Server who will be receiving messages");

static int nmp_in_port = 3;
module_param(nmp_in_port, int, 0764);
MODULE_PARM_DESC(nmp_in_port, "OVS in_port for nmp_iface");

/* Global handlers for cpu stats thread and report dev */
struct net_device  *nm_dev = NULL;
struct task_struct *t;

/* OVS hook */
extern int (*ovs_hook)(struct sw_flow *flow, struct sk_buff *skb);

/* static u8  broadcast_mac[6] = {0xff,0xff,0xff,0xff,0xff,0xff}; */
static u8  server_mac[6] = {0x0,0x0,0x0,0x0,0x0,0x0};
static u8  local_mac[6] = {0x0,0x0,0x0,0x0,0x0,0x0};
static u8  net_msgbuff[128];
static u8  cpu_msgbuff[128];
static u32 report_id = 0;

const static unsigned short eth_type = htons(ETH_P_NETMON);

/** 
 *
 * Util funcs
 *
 **/
static int
nm_alloc_skb(struct sk_buff **rskb, int size) {
	struct sk_buff *skb;

	if (!nm_dev)
		return -1;

	/* Initialization */
	*rskb = alloc_skb(size, GFP_ATOMIC);
	skb = *rskb;
	if (!(skb))
		return -1;

	skb->sk = NULL;
	skb->pkt_type = PACKET_OTHERHOST;
	/* checksum: we dont need any checksum */
	skb->ip_summed = CHECKSUM_NONE;

	skb->dev = nm_dev;

	return 0;
}

u16
nm_skb_proto(struct sk_buff *skb) {
	struct ethhdr *eth = (struct ethhdr *) &skb->data[0];
	return htons(eth->h_proto);
}

u64 nsecs_to_jiffies64(u64 n)
{
#if (NSEC_PER_SEC % HZ) == 0
	/* Common case, HZ = 100, 128, 200, 250, 256, 500, 512, 1000 etc. */
	return div_u64(n, NSEC_PER_SEC / HZ);
#elif (HZ % 512) == 0
	/* overflow after 292 years if HZ = 1024 */
	return div_u64(n * HZ / 512, NSEC_PER_SEC / 512);
#else
	/*
	 * Generic case - optimized for cases where HZ is a multiple of 3.
	 * overflow after 64.99 years, exact for HZ = 60, 72, 90, 120 etc.
	 */
	return div_u64(n * 9, (9ull * NSEC_PER_SEC + HZ / 2) / HZ);
#endif
}


/** 
 *
 * NET stats
 *
 **/
static int
nm_report_net_stats(struct sw_flow *flow, struct sk_buff *fskb) {
	struct netmonmsg *m = (struct netmonmsg *) &net_msgbuff[0];
	struct sk_buff *skb;

	static int pkt_size = sizeof(struct ethhdr) + 
		      			  sizeof(struct netmonmsg) +
						  sizeof(struct net_stats) + 32;
	static int dat_size = sizeof(struct netmonmsg) +
						  sizeof(struct net_stats);
	
	do_gettimeofday(&m->ts);
	
	m->s_type                = S_NET;
	m->sender                = report_id;
	m->st[0].net[0].packets  = flow->packet_count;
	m->st[0].net[0].bytes    = flow->byte_count;
	m->st[0].net[0].addr.src = flow->key.ipv4.addr.src;
	m->st[0].net[0].addr.dst = flow->key.ipv4.addr.dst;
	m->st[0].net[0].tp.src   = flow->key.ipv4.tp.src;
	m->st[0].net[0].tp.dst   = flow->key.ipv4.tp.dst;
	
	flow->reported = jiffies;

	if (nm_alloc_skb(&skb, pkt_size) < 0) {
		printk(KERN_ALERT "err: could not alloc skb");
		return -1;
	}

	memcpy(skb_put(skb, 6), &server_mac, 6);
	memcpy(skb_put(skb, 6), &local_mac, 6);
	memcpy(skb_put(skb, 2), &eth_type, 2);
	memcpy(skb_put(skb, dat_size), net_msgbuff, dat_size);
	
	if (dev_queue_xmit(skb) != NET_XMIT_SUCCESS) {
		printk(KERN_ALERT "err: dev_queue_xmit");
		return -1;
	}

	return 0;
}
/** 
 *
 * CPU stats
 *
 **/

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,5,0)

static cputime64_t get_iowait_time(int cpu)
{
       return kcpustat_cpu(cpu).cpustat[CPUTIME_IOWAIT];
       /* if (cpu_online(cpu) && nr_iowait_cpu(cpu))
                iowait += arch_idle_time(cpu); 
        return iowait; */
}

static u64 get_idle_time(int cpu)
{
        u64 idle, idle_time = -1ULL;

        if (cpu_online(cpu))
                idle_time = get_cpu_idle_time_us(cpu, NULL);

        if (idle_time == -1ULL)
                /* !NO_HZ or cpu offline so we can rely on cpustat.idle */
                idle = kcpustat_cpu(cpu).cpustat[CPUTIME_IDLE];
        else
                idle = usecs_to_cputime64(idle_time);

        return idle;
}

#else

static cputime64_t get_idle_time(int cpu)
{
        u64 idle_time = get_cpu_idle_time_us(cpu, NULL);
        cputime64_t idle = cputime64_zero;

        if (idle_time == -1ULL) {
                /* !NO_HZ so we can rely on cpustat.idle */
                idle = kstat_cpu(cpu).cpustat.idle;
                idle = cputime64_add(idle, arch_idle_time(cpu));
        } else
                idle = usecs_to_cputime64(idle_time);

        return idle;
}

static cputime64_t get_iowait_time(int cpu)
{
        u64 iowait_time = get_cpu_iowait_time_us(cpu, NULL);
        cputime64_t iowait = cputime64_zero;

        if (iowait_time == -1ULL)
                /* !NO_HZ so we can rely on cpustat.iowait */
                iowait = kstat_cpu(cpu).cpustat.iowait;
        else
                iowait = usecs_to_cputime64(iowait_time);

        return iowait;
}
#endif /* LINUX_VERSION_CODE >= KERNEL_VERSION(3,5,0) */

int
cpu_stats_diff (struct cpu_stats *c1, struct cpu_stats *c2) {
	return 1;
}

static int
nm_report_cpu_stats(void) {
	/* For cpu stats */

	struct netmonmsg *m = (struct netmonmsg *) &cpu_msgbuff[0];
	struct sk_buff *skb;
	struct cpu_stats cpu_s;

	static int pkt_size = sizeof(struct ethhdr) + 
		      			  sizeof(struct netmonmsg) +
						  sizeof(struct cpu_stats) + 32;
	static int dat_size = sizeof(struct netmonmsg) +
						  sizeof(struct cpu_stats);

	cputime64_t user, nice, system, idle, iowait, irq, softirq, steal;
	cputime64_t guest, guest_nice;
	int i;
	/* u64 sum = 0; */
    /* u64 sum_softirq = 0; */
	/* unsigned int per_softirq_sums[NR_SOFTIRQS] = {0}; */
	
	/**
	 *
	 * Code from fs/proc/stat.c
	 *
	 **/	
    user = nice    = system = idle = iowait =
     irq = softirq = steal  = cputime64_zero;
    guest = guest_nice = cputime64_zero;
	
	
#if LINUX_VERSION_CODE <= KERNEL_VERSION(3,7,0)
	for_each_possible_cpu(i) {
	    cpu_s.t_us = cputime64_add(cpu_s.t_us, kstat_cpu(i).cpustat.user);
	    cpu_s.t_sy = cputime64_add(cpu_s.t_sy, kstat_cpu(i).cpustat.system);
	    cpu_s.t_id = cputime64_add(cpu_s.t_id, get_idle_time(i));
	    cpu_s.t_wa = cputime64_add(cpu_s.t_wa, get_iowait_time(i));
	    cpu_s.t_hi = cputime64_add(cpu_s.t_hi, kstat_cpu(i).cpustat.irq);
	    cpu_s.t_st = cputime64_add(cpu_s.t_st, kstat_cpu(i).cpustat.softirq);
	    /* steal = cputime64_add(steal, kstat_cpu(i).cpustat.steal);
	    guest = cputime64_add(guest, kstat_cpu(i).cpustat.guest);
	    guest_nice = cputime64_add(guest_nice,
	            kstat_cpu(i).cpustat.guest_nice); */

		/* Don't need irq stats for now */
	    /* sum += kstat_cpu_irqs_sum(i); */
	    /* sum += arch_irq_stat_cpu(i); */

	    /* for (j = 0; j < NR_SOFTIRQS; j++) {
	            unsigned int softirq_stat = kstat_softirqs_cpu(j, i);

	            per_softirq_sums[j] += softirq_stat;
	            sum_softirq += softirq_stat;
	    } */
	}
#else
	for_each_possible_cpu(i) {
		
	    cpu_s.t_us += kcpustat_cpu(i).cpustat[CPUTIME_USER];
	    cpu_s.t_sy += kcpustat_cpu(i).cpustat[CPUTIME_SYSTEM];
	    cpu_s.t_id += get_idle_time(i);
	    cpu_s.t_wa += get_iowait_time(i);
	    cpu_s.t_hi += kcpustat_cpu(i).cpustat[CPUTIME_IRQ];
	    cpu_s.t_st += kcpustat_cpu(i).cpustat[CPUTIME_SOFTIRQ];		
		
		/* Don't need irq stats for now */
	    /* sum += kstat_cpu_irqs_sum(i); */
	    /* sum += arch_irq_stat_cpu(i); */

	    /* for (j = 0; j < NR_SOFTIRQS; j++) {
	            unsigned int softirq_stat = kstat_softirqs_cpu(j, i);

	            per_softirq_sums[j] += softirq_stat;
	            sum_softirq += softirq_stat;
	    } */
	}
#endif
	do_gettimeofday(&m->ts);
	
	if (!cpu_stats_diff(&m->st[0].cpu[0], &cpu_s))
		return 0;
	
	m->s_type             = S_CPU;
	m->sender             = report_id;	
	m->st[0].cpu[0].t_us  = cpu_s.t_us;
	m->st[0].cpu[0].t_sy  = cpu_s.t_sy;
	m->st[0].cpu[0].t_id  = cpu_s.t_id;
	m->st[0].cpu[0].t_wa  = cpu_s.t_wa;
	m->st[0].cpu[0].t_hi  = cpu_s.t_hi;
	m->st[0].cpu[0].t_st  = cpu_s.t_st;

	if (nm_alloc_skb(&skb, pkt_size) < 0) {
		printk(KERN_ALERT "err: could not alloc skb");
		return -1;
	}
	memcpy(skb_put(skb, 6), &server_mac, 6);
	memcpy(skb_put(skb, 6), &local_mac, 6);
	memcpy(skb_put(skb, 2), &eth_type, 2);
	memcpy(skb_put(skb, dat_size), cpu_msgbuff, dat_size);

	if (dev_queue_xmit(skb) != NET_XMIT_SUCCESS) {
		printk(KERN_ALERT "err: dev_queue_xmit");
		return -1;
	}

	return 0;
}

static int
nm_report_cpu_thread(void *data) {
	while(!kthread_should_stop()) {
		nm_report_cpu_stats();
		msleep(nmp_cpu_interval);
	}
	return 0;
}
/** 
 *
 * END CPU stats
 *
 **/

/* static int
nm_server_lookup(struct net_device *dev) {
	static unsigned short eth_type = htons(ETH_P_NETMON_Q);
	struct sk_buff *skb;

	if (nm_alloc_skb(&skb, 128) < 0) {
		printk(KERN_INFO "could not alloc skb\n");
		return -1;
	}
	
	memcpy(skb_put(skb, 6), &broadcast_mac, 6);
	memcpy(skb_put(skb, 6), dev->dev_addr, 6);
	memcpy(skb_put(skb, 2), &eth_type, 2);

	skb->dev = dev;

	if (dev_queue_xmit(skb) != NET_XMIT_SUCCESS) {
		printk(KERN_ALERT "err: dev_queue_xmit");
		return -1;
	}
	return 0;
} */

/**
 *
 *  Packet processing hook
 *
 **/
int
nm_process_packet(struct sw_flow *flow, struct sk_buff *skb) {
	struct ethhdr *eth;
retry:
	if (0 && is_zero_ether_addr(server_mac)) {
		/* Check if this packet is an announcement */
		if (nm_skb_proto(skb) == ETH_P_NETMON) {
			eth = (struct ethhdr *) skb->data;
			if (compare_ether_addr(eth->h_dest,local_mac) == 0) {
				/* Answer to my previous query */
				memcpy(&server_mac, eth->h_dest, ETH_ALEN);
				report_id = ntohl(*(u32*)(eth+1));
				printk("Got id %u from "ETH_ADDR_FMT"\n", report_id, 
					   ETH_ADDR_ARGS(server_mac));
				goto retry;
			}
		}
	}
	else {
		if (flow && (flow->reported + nmp_net_interval < jiffies) && 
			nu_is_ip(flow->key.eth.type)) {
			/* Report only flows directed to me */
			if ((compare_ether_addr(flow->key.eth.dst,local_mac) == 0) ||
				(flow->key.phy.in_port == nmp_in_port))
				return nm_report_net_stats(flow, skb);
		}
		else {
			/* if (!flow) {
				if (printk_ratelimit())
					printk(KERN_ALERT "null flow!\n");
			}
			else {
				if (printk_ratelimit()) {
					printk(KERN_ALERT "not null but type 0x%x!\n",
						   flow->key.eth.type);	
					if (flow->reported + nmp_net_interval < jiffies)
						printk(KERN_ALERT "time issues %lx, %lx, %lx!\n", 
							   flow->reported, nmp_net_interval, jiffies);
				}
			} */
			/* if (!flow)
				printk(KERN_INFO "no flow");
			else if (flow->reported + HZ >= jiffies) {
				printk(KERN_INFO "reported %lx, jiffies %lx\n", flow->reported, jiffies);
			}
			else if (!nu_is_ip(flow->key.eth.type)) {
				printk(KERN_INFO "eth.type is %x\n", flow->key.eth.type);
			} */
			
			/* For now let ovs handle the rest of traffic and do nothing
			   if flow isn't knwon
			 */
			/* if (nu_frame_is_ip(&skb->data[0])) {
				printk(KERN_INFO "is ip");
			}
			else {
				printk(KERN_INFO "not ip");
			} */
		}
	}
	return 0;
}



int
init_module(void)
{
	nm_dev = dev_get_by_name(&init_net, nmp_iface);
	if (!nm_dev) {
		printk(KERN_INFO "Could not find network interface");
		return -ENXIO;
	}
	
	sscanf(nmp_server_mac, ETH_ADDR_SCAN_FMT, ETH_ADDR_SCAN_ARGS(server_mac));

   report_id = nmp_report_id;
	/* Algorithm:
		Figure out my mac address, given iface */

	memcpy(&local_mac, nm_dev->dev_addr, ETH_ALEN);
	printk(KERN_INFO "Netmon loaded for iface %s: "ETH_ADDR_FMT"\n"
		   "Server MAC is "ETH_ADDR_FMT" and report_id is %d.\n"
		   "Reporting cpu %d ms, flows %d jiffies\n", 
		   nmp_iface, ETH_ADDR_ARGS(nm_dev->dev_addr),
		   ETH_ADDR_ARGS(server_mac), report_id,
		   nmp_cpu_interval,
		   nmp_net_interval);

	/* Set hook */
	ovs_hook = nm_process_packet;
	
	/* Send message looking for server */
    /* nm_server_lookup(nm_dev); */
	
	/*  Create thread to monitor CPU */
    t = kthread_create(&nm_report_cpu_thread, NULL, "netmon_cpu_t");
    /* kthread_bind(t,swt_cpu); */

	wake_up_process(t);
	return 0;
}


void
cleanup_module(void)
{
	if (nm_dev)
		dev_put(nm_dev);
	if (kthread_stop(t) < 0)
		printk(KERN_INFO "couldn't change thread status\n");
	/* unset the hook */
	ovs_hook = NULL;
}

MODULE_LICENSE("GPL");
