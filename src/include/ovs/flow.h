struct ovs_key_ipv4_tunnel {
	__be64 tun_id;
	__u32  tun_flags;
	__be32 ipv4_src;
	__be32 ipv4_dst;
	__u8   ipv4_tos;
	__u8   ipv4_ttl;
	__u8   pad[2];
};

struct sw_flow_key {
        struct ovs_key_ipv4_tunnel tun_key;  /* Encapsulating tunnel key. */
        struct {
                u32     priority;       /* Packet QoS priority. */
                u32     skb_mark;       /* SKB mark. */
                u16     in_port;        /* Input switch port (or DP_MAX_PORTS). */
        } phy;
        struct {
                u8     src[ETH_ALEN];   /* Ethernet source address. */
                u8     dst[ETH_ALEN];   /* Ethernet destination address. */
                __be16 tci;             /* 0 if no VLAN, VLAN_TAG_PRESENT set otherwise. */
                __be16 type;            /* Ethernet frame type. */
        } eth;
        struct {
                u8     proto;           /* IP protocol or lower 8 bits of ARP opcode. */
                u8     tos;             /* IP ToS. */
                u8     ttl;             /* IP TTL/hop limit. */
                u8     frag;            /* One of OVS_FRAG_TYPE_*. */
        } ip;
        union {
                struct {
                        struct {
                                __be32 src;     /* IP source address. */
                                __be32 dst;     /* IP destination address. */
                        } addr;
                        union {
                                struct {
                                        __be16 src;             /* TCP/UDP source port. */
                                        __be16 dst;             /* TCP/UDP destination port. */
                                } tp;
                                struct {
                                        u8 sha[ETH_ALEN];       /* ARP source hardware address. */
                                        u8 tha[ETH_ALEN];       /* ARP target hardware address. */
                                } arp;
                        };
                } ipv4;
                struct {
                        struct {
                                struct in6_addr src;    /* IPv6 source address. */
                                struct in6_addr dst;    /* IPv6 destination address. */
                        } addr;
                        __be32 label;                   /* IPv6 flow label. */
                        struct {
                                __be16 src;             /* TCP/UDP source port. */
                                __be16 dst;             /* TCP/UDP destination port. */
                        } tp;
                        struct {
                                struct in6_addr target; /* ND target address. */
                                u8 sll[ETH_ALEN];       /* ND source link layer address. */
                                u8 tll[ETH_ALEN];       /* ND target link layer address. */
                        } nd;
                } ipv6;
        };
};

struct sw_flow {
	struct rcu_head rcu;
	struct hlist_node hash_node[2];
	u32 hash;

	struct sw_flow_key key;
	struct sw_flow_actions __rcu *sf_acts;

	spinlock_t lock;	/* Lock for values below. */
	unsigned long used;	/* Last used time (in jiffies). */
	u64 packet_count;	/* Number of packets matched. */
	u64 byte_count;		/* Number of bytes matched. */
	u8 tcp_flags;		/* Union of seen TCP flags. */
	
	unsigned long reported; /* Last time reported */
	/* TODO: Precise stats */
	/* u64 byte_sec[4]; */   /* Bytes matched last 4 time intervals */
	/* u64 byte_sec_time; */ /* Timestamp of last time interval */
	/* u8 byte_sec_head; */  /* Head ptr of byte_sec array */
};
