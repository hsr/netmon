#ifndef _NET_UTIL_H_
#define _NET_UTIL_H_

#ifndef __KERNEL__
#include <inttypes.h>
#include <linux/types.h>
#else
#define 	PRIx8   "x"
#define 	PRIX8   "X"
#define     PRIu8   "u"

#define 	SCNx8   "x"
#define 	SCNX8   "X"
#define 	SCNu8   "u"
#endif

#include "util.h"

int nu_get_mac (char *ifname, uchar *mac);
int nu_get_ifindex (char *ifname);

/* Example:
 *
 * char *string = "1 00:11:22:33:44:55 2";
 * uint8_t mac[ETH_ADDR_LEN];
 * int a, b;
 *
 * if (sscanf(string, "%d"ETH_ADDR_SCAN_FMT"%d",
 *     &a, ETH_ADDR_SCAN_ARGS(mac), &b) == 1 + ETH_ADDR_SCAN_COUNT + 1) {
 *     ...
 * }
 */
#define ETH_ADDR_SCAN_FMT "%02hh"SCNx8":%02hh"SCNx8":%02hh"SCNx8":%02hh"SCNx8":%02hh"SCNx8":%02hh"SCNx8
#define ETH_ADDR_SCAN_ARGS(ea) \
        &(ea)[0], &(ea)[1], &(ea)[2], &(ea)[3], &(ea)[4], &(ea)[5]


/* Example:
 *
 * uint8_t mac[ETH_ADDR_LEN];
 *    [...]
 * printf("The Ethernet address is "ETH_ADDR_FMT"\n", ETH_ADDR_ARGS(mac));
 *
 */
#define ETH_ADDR_FMT                                                    \
    "%02"PRIx8":%02"PRIx8":%02"PRIx8":%02"PRIx8":%02"PRIx8":%02"PRIx8
#define ETH_ADDR_ARGS(ea)                                   \
    (ea)[0], (ea)[1], (ea)[2], (ea)[3], (ea)[4], (ea)[5]


/* The "(void) (ip)[0]" below has no effect on the value, since it's the first
 * argument of a comma expression, but it makes sure that 'ip' is a pointer.
 * This is useful since a common mistake is to pass an integer instead of a
 * pointer to IP_ARGS. */
#define IP_FMT "%"PRIu8".%"PRIu8".%"PRIu8".%"PRIu8
#define IP_ARGS(ip)                             \
        ((void) (ip)[0], ((uint8_t *) ip)[0]),  \
        ((uint8_t *) ip)[1],                    \
        ((uint8_t *) ip)[2],                    \
        ((uint8_t *) ip)[3]

/* Example:
 *
 * char *string = "1 33.44.55.66 2";
 * ovs_be32 ip;
 * int a, b;
 *
 * if (sscanf(string, "%d"IP_SCAN_FMT"%d",
 *     &a, IP_SCAN_ARGS(&ip), &b) == 1 + IP_SCAN_COUNT + 1) {
 *     ...
 * }
 */
#define IP_SCAN_FMT "%"SCNu8".%"SCNu8".%"SCNu8".%"SCNu8
#define IP_SCAN_ARGS(ip)                                    \
        ((void) (ovs_be32) *(ip), &((uint8_t *) ip)[0]),    \
        &((uint8_t *) ip)[1],                               \
        &((uint8_t *) ip)[2],                               \
        &((uint8_t *) ip)[3]
#define IP_SCAN_COUNT 4
			
inline int nu_is_ip(u16 eth_type);
inline int nu_frame_type(u8 *data, u16 eth_type);
inline int nu_frame_is_ip(u8 *data);
inline void nu_print_eth(u8 *data);


#ifdef __KERNEL__
inline int
nu_is_ip(u16 eth_type) {
	return (ntohs(eth_type) == 0x0800);
}

inline int nu_frame_type(u8 *data, u16 cmp_type) {
	u16 eth_type = (u16) data[12];
	return (ntohs(eth_type) == cmp_type);
}
	
inline int
nu_frame_is_ip(u8 *data) {
	return nu_frame_type(data, 0x0800);
}


inline void
nu_print_eth(u8 *data) {
#ifdef __KERNEL__
	printk(KERN_INFO "pkt from "ETH_ADDR_FMT" -> "ETH_ADDR_FMT,
		ETH_ADDR_ARGS(&data[6]),ETH_ADDR_ARGS(&data[0]));
#else
	printf("pkt from "ETH_ADDR_FMT" -> "ETH_ADDR_FMT"\n",
		   ETH_ADDR_ARGS(&data[6]),ETH_ADDR_ARGS(&data[0]));
#endif
}

#endif

#endif /* _NET_UTIL_H_ */
