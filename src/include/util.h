#ifndef __HSR_UTIL_H__
#define __HSR_UTIL_H__

#include <linux/version.h>

typedef unsigned char      uchar;
#if !defined(__KERNEL__) || (LINUX_VERSION_CODE < KERNEL_VERSION(3,5,0))
typedef unsigned int       uint;
typedef uchar              u8;
typedef unsigned short     u16;
typedef unsigned int       u32;
typedef unsigned long long u64;
#endif

void eexit(char *msg);

void warn(char *msg);

#endif /* __HSR_UTIL_H__ */
