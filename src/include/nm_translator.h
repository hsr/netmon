#ifndef _NM_TRANSLATOR_H_
  #define _NM_TRANSLATOR_H_


#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#include <netinet/ip.h>
#include <net/if.h>
#include <net/ethernet.h>
#include <linux/if_ether.h>   // ETH_P_ARP = 0x0806, ETH_P_ALL = 0x0003

#include "netmon.h"
#include "uthash.h"

struct nm_query_msg {
  u32  id;
};

void * nm_translator(void *iface);
  
  #endif /* _NM_TRANSLATOR_H_ */
