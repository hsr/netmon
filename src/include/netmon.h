#ifndef _NETMON_H_
#define _NETMON_H_

#define ETH_P_NETMON 0xf370   /* Netmon cpu stats msg */

#include "util.h"

struct net_stats {
  struct {
		__be32 src;	/* IP source address. */
		__be32 dst;	/* IP destination address. */
  } addr;
  struct {
	__be16 src;		/* TCP/UDP source port. */
	__be16 dst;		/* TCP/UDP destination port. */
  } tp;
  int64_t bytes;
  int64_t packets;
};

struct cpu_stats {
  int64_t t_us;                 /* User time */
  int64_t t_sy;                 /* System time */
  int64_t t_id;                 /* Idle time */
  int64_t t_wa;                 /* Time waiting for IO */
  int64_t t_hi;                 /* Time for hard interrupt */
  int64_t t_st;                 /* Time for soft interrupt */
};

struct nm_stats {
	union {
	    struct cpu_stats cpu[0];
	    struct net_stats net[0];
	};
};

enum {
	S_CPU = 1,
	S_NET = 2
};

struct netmonmsg {
  struct timeval  ts;           /* Timestamp */
  int64_t         sender;
  unsigned short  s_type;       /* CPU or NET stats */
  struct nm_stats st[0];        /* Stats */
};

/* Netmon database structure */
struct database {
	struct timeval   tv_start;
	struct timeval   tv_end;
	u32              tail;
	u32              head;
	u32              total;
	struct netmonmsg m[0];
};

#endif /* _NETMON_H_ */

