#!/bin/bash

source /home/hsr/.profile

TRANSFERS=4
EXP_NAME=random

for RATE in 1000 500 250; do
	echo "========================Rate: ${RATE} $(date +%s)========================"
	echo "Setting rate..."
	sudo /home/hsr/Code/misc/net-scripts/rate_limit.py -r ${RATE} -i eth2 -m dcswitch120,dcswitch121,dcswitch122,dcswitch123,dcswitch124,dcswitch140;

	echo "Starting random flows..."
	for i in $(seq 120 124); do
		 ssh random${i}-1 -f '~/rand-flow.py -v -t '${TRANSFERS}' -S random120-1,random121-1,random122-1,random123-1,random124-1'
	 done

	echo "Starting database..."
	ssh dcswitch125 -f 'sudo /home/hsr/Code/ucsd/netmon/src/user/nm_database.py -v -i br0 -F 2 -D 4000 -A 1000 > /tmp/exp_'${EXP_NAME}'_'${RATE}'.txt' &
	PID=$!
	echo "Connection PID ${PID}"
	
	echo "Removing old data from dfs..."	
	ssh hadoop-head 'source /home/seelab/hadoop-util/scripts/config.sh; sudo -u hduser ${HADOOP_HOME}/hadoop dfs -rmr ${EXP_DIR}/${INPUT_DIR}-output'
	
	echo "Running job."
	ssh hadoop-head 'source /home/seelab/hadoop-util/scripts/config.sh; sudo -u hduser ${HADOOP_HOME}/hadoop jar /home/seelab/random-src/build.jar InvertedIndex ${EXP_DIR}/${INPUT_DIR} ${EXP_DIR}/${INPUT_DIR}-output &> /tmp/exp_job_'${EXP_NAME}'_'${RATE}'.txt'
	
	sleep 20
	
	echo "Killing all"
	for pid in $(ps aux | grep -E '(ssh\ dcswitch125|ssh\ random)' | grep -v grep | awk '{print $2}'); do 
		echo "Killing ${pid}"
		kill $pid;
	done
	echo "========================END Rate: ${RATE} $(date +%s)========================"
done