#!/usr/bin/env python
"""
SYNOPSIS
 
    Usage: %s <id> <input> <output>
 
DESCRIPTION
 
    Netmon database. Collects data from netmon clients and takes decisions about
    network allocation

"""


import sys
__doc__ = __doc__ % sys.argv[0]

import subprocess
import optparse
import traceback
import os

import time
import numpy as np
import operator
from threading import Thread
from datetime import datetime as dt

from boomslang import *

# third party code
script_dir = os.path.dirname(os.path.realpath(__file__));
sys.path.insert(0, '%s/third/' % script_dir);
from util import *

import nmparser

def main():
    global options
    
    lines = nmparser.getFileLines(sys.argv[2])
    cpuLoad = nmparser.getCPU(lines, relative=True)
    Xvalues, USvalues, SYvalues, X0 = cpuLoad[0], cpuLoad[1], cpuLoad[2], cpuLoad[3]
    # Xtrash, totalBytes = getNET()
    
    plot  = Plot()
    
    plot.xLabel = "Time (s)"
    plot.yLabel = "Network Flow Activity (bytes)"

    lineColors  = ['purple','blue', 'green', 'orange', 'pink']
    lineStyles  = ['-.']
    lineColorsI = 0
    lineStylesI = 0
    
    #print X0
    lastNetReport = 0
    cnt = 0
    flows = (nmparser.getFlows(lines, X0, relative=True))
    
    
    
    detectedFlows=nmparser.getDetectedFlows(lines)
    #print 'detected:', detectedFlows.values()
    detectedFlows=detectedFlows.values()

    sums = [(k,sum(v)) for k,v in flows[2].iteritems()]
    sums = sorted(sums, key=lambda tup: tup[1])
    if len(sums) > 20:
        sums = sums[-20:]
    displayFlows = [e[0] for e in sums]
        
    for k,v in flows[0].iteritems():
        if k not in displayFlows:
            continue
        fdesc = v.split('->')
        fsrc  = fdesc[0].split(':')
        fdst  = fdesc[1].split(':')
        fsrc  = '.'.join(fsrc[0].split('.')[2:4] + [fsrc[1]])
        fdst  = '.'.join(fdst[0].split('.')[2:4] + [fdst[1]])
        label = fsrc+'->'+fdst

        if sum(flows[2][k]) > 1e6:
            line = Line()
            line.xValues = flows[1][k]
            if line.xValues[-1] > lastNetReport:
                lastNetReport = line.xValues[-1]
            line.yValues = flows[2][k]
            line.xValues += [line.xValues[-1]+.000001]
            line.yValues += [0]
            #line.label = label
            line.label = 'Flow %d' % (cnt+1) #label
            print 'Flow %d' % (cnt+1), '%s' % str(v)
            
            
            #print line.yValues
            
            line.lineStyle = lineStyles[lineStylesI]
            line.color = lineColors[lineColorsI]
            if lineStylesI == (len(lineStyles)-1):
                lineColorsI = (lineColorsI + 1) % len(lineColors);
            lineStylesI = (lineStylesI + 1) % len(lineStyles)

            if v in detectedFlows:
                line.marker = 'v'
                line.markerSize = 8
                #line.lineWidth = 4

            
            #print 'adding', label
            plot.add(line)
            cnt += 1

    line = Line()
    lastCPUReportI = len(Xvalues)-1
    for i in xrange(0,lastCPUReportI):
        if Xvalues[lastCPUReportI-i] < lastNetReport+50:
            break
    print i, lastCPUReportI-i, Xvalues[(lastCPUReportI-i)], lastNetReport
    Xvalues        = Xvalues[:(lastCPUReportI-i)]
    USvalues       = USvalues[:(lastCPUReportI-i)]
    
    line.xValues   = Xvalues
    line.yValues   = USvalues
    line.label     = "CPU"
    line.lineStyle = "-"
    line.marker    = 'o'
    line.markerSize = 4
    line.color     = "red"
    plot.add(line)
    
    plot.hasLegend(labelSize=12, columns=3)
    
    
    increaseLines = VLine()
    increaseLines.xValues = \
        nmparser.getCPUIncreaseTimes(lines,X0,relative=True,limit=-1)[0]
    increaseLines.color = 'DarkGrey'
    plot.add(increaseLines)
    
    plot.setTwinX("CPU Time (Kernel Jiffies)", cnt)
    
    plot.axesLabelSize = 18
    
    #plot.save(sys.argv[3])

    
    layout = PlotLayout()
    layout.setPlotParameters(left=.14, right=.9, top=.95)
    #layout.axesLabelSize = 18
    layout.addPlot(plot)
    layout.save(sys.argv[3])


    
    return 0;

if __name__ == '__main__':
    try:
        parser = optparse.OptionParser(
            formatter=optparse.TitledHelpFormatter(),
            usage=globals()['__doc__'],
            version='')
        
        if len(sys.argv) < 4:
            print __doc__
            sys.exit()
        sys.exit(main())

    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        print 'Unexpected exception "%s"' % str(e)
        traceback.print_exc()
        sys.exit(1)
