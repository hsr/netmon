#!/usr/bin/env python
"""
SYNOPSIS
 
    Usage: %s <id> <input> <output>
 
DESCRIPTION
 
    Netmon database. Collects data from netmon clients and takes decisions about
    network allocation

"""


import sys
__doc__ = __doc__ % sys.argv[0]

import subprocess
import optparse
import traceback
import os

import time
import numpy as np
from threading import Thread
from datetime import datetime as dt

from boomslang import *

# third party code
script_dir = os.path.dirname(os.path.realpath(__file__));
sys.path.insert(0, '%s/third/' % script_dir);
from util import *

import nmparser

def main():
    global options
    
    lines             = nmparser.getFileLines(sys.argv[2])
    cpuLoad           = nmparser.getCPU(lines, relative=False)
    increase          = nmparser.getCPUIncrease(lines, cpuLoad[3],
                                                 relative=False)
    Xvalues, USvalues = cpuLoad[0], cpuLoad[1]
    SYvalues, X0      = cpuLoad[2], cpuLoad[3]

    
    print increase
    
    plot  = Plot()
    
    plot.xLabel = "Time"
    plot.yLabel = "First Plot's Y Axis"
    #plot.hasLegend()

    # line = Line()
    # line.xValues = Xvalues
    # line.yValues = SYvalues
    # line.label = "System Time"
    # line.lineStyle = "--"
    # line.color = "blue"
    # plot.add(line)
    
    lineColors  = ['purple','blue']
    lineStyles  = [':','-.']
    lineColorsI = 0
    lineStylesI = 0
    
    print X0
    cnt = 0
    flows = (nmparser.getFlows(lines, X0, relative=False))
    for k,v in flows[0].iteritems():
        fdesc = v.split('->')
        fsrc  = fdesc[0].split(':')
        fdst  = fdesc[1].split(':')
        fsrc  = '.'.join(fsrc[0].split('.')[2:4] + [fsrc[1]])
        fdst  = '.'.join(fdst[0].split('.')[2:4] + [fdst[1]])
        label = fsrc+'->'+fdst

        if sum(flows[2][k]) > 84778.0:
            line = Line()
            line.xValues = flows[1][k]
            line.yValues = flows[2][k]
            line.xValues += [line.xValues[-1]+.000001]
            line.yValues += [0]
            line.label = label
            
            line.lineStyle = lineStyles[lineStylesI]
            line.color = lineColors[lineColorsI]
            if lineStylesI == (len(lineStyles)-1):
                lineColorsI = (lineColorsI + 1) % len(lineColors);
            lineStylesI = (lineStylesI + 1) % len(lineStyles)
            
            #print 'adding', label
            plot.add(line)
            cnt += 1
        # else:
        #     print 'skipping', label

    line = Line()
    line.xValues = Xvalues
    line.yValues = USvalues
    line.label = "User time"
    line.lineStyle = "-"
    line.marker('D')
    line.color = "red"
    plot.add(line)
    
    plot.setTwinX("Second Plot's Y Axis", cnt)
    

    plot.save(sys.argv[3])
    
    return 0;

if __name__ == '__main__':
    try:
        parser = optparse.OptionParser(
            formatter=optparse.TitledHelpFormatter(),
            usage=globals()['__doc__'],
            version='')
        
        if len(sys.argv) < 4:
            print __doc__
            sys.exit()
        sys.exit(main())

    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        print 'Unexpected exception "%s"' % str(e)
        traceback.print_exc()
        sys.exit(1)
