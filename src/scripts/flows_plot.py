#!/usr/bin/env python
"""
SYNOPSIS
 
    Usage: %s <id> <input> <output>
 
DESCRIPTION
 
    Netmon database. Collects data from netmon clients and takes decisions about
    network allocation

"""


import sys
__doc__ = __doc__ % sys.argv[0]

import subprocess
import optparse
import traceback
import os

import time
import numpy as np
from threading import Thread
from datetime import datetime as dt

from boomslang import *

# third party code
script_dir = os.path.dirname(os.path.realpath(__file__));
sys.path.insert(0, '%s/third/' % script_dir);
from util import *

def getFileLines(filename = sys.argv[2]):
    f = open(filename,'r')
    lines = f.readlines()
    f.close()
    return lines


def getCPU(lines):
    X = []
    yus  = []
    ysy= []
    for l in lines:
        if l[0:len('c(%s)' % sys.argv[1])] == ('c(%s)' % sys.argv[1]):
            c = l.split()
            #print (c[1], c[2][3:], c[3][3:])
            X += [float(c[1])]
            yus += [float(c[2][3:])]
            ysy += [float(c[3][3:])]
    
    Xvalues = []
    USvalues = []
    SYvalues = []
    X0 = X[0]
    for i in xrange(1,len(X)):
        Xvalues += [X[i]-X0]
        USvalues += [yus[i] - yus[i-1]]
        SYvalues += [ysy[i] - ysy[i-1]]
    
    # for i in xrange(0,len(Xvalues)):
    #     print Xvalues[i], USvalues[i], SYvalues[i]
    
    #print Xvalues
    return Xvalues, USvalues, SYvalues 

def getFlows(lines):
    FlowsDesc = {}
    FlowsX = {}
    FlowsY = {}
    X0 = -1
    for l in lines:
        if l[0:len('n(%s)' % sys.argv[1])] == ('n(%s)' % sys.argv[1]):
            c = l.split()
            #print (c[1], c[2][3:], c[3][3:])
            flow = c[2]+'->'+c[4]
            flow_hash = hash(flow)
            FlowsDesc[flow_hash]  = flow
            
            if X0 == -1:
                X0 = float(c[1])
            
            if not FlowsX.has_key(flow_hash):
                FlowsX[flow_hash] = []
            FlowsX[flow_hash]    += [float(c[1])]
            
            if not FlowsY.has_key(flow_hash):
                FlowsY[flow_hash] = []
            FlowsY[flow_hash]    += [float(c[5][1:])]
    X = {}
    Y = {}
    for flow_hash in FlowsX.keys():
        for i in xrange(1,len(FlowsX[flow_hash])):

            if not X.has_key(flow_hash):
                X[flow_hash] = []
            X[flow_hash] += [FlowsX[flow_hash][i]-X0]

            if not Y.has_key(flow_hash):
                Y[flow_hash] = []
            Y[flow_hash] += [FlowsY[flow_hash][i]-FlowsY[flow_hash][i-1]]
    
    return (FlowsDesc, X, Y)

def main():
    global options
    
    lines = getFileLines(sys.argv[2])
    Xvalues, USvalues, SYvalues = getCPU(lines)
    # Xtrash, totalBytes = getNET()
    
    plot  = Plot()
    
    plot.xLabel = "Time"
    plot.yLabel = "bytes"
    
    plot.addLineColor("red")
    plot.addLineColor("blue")
    plot.addLineColor("green")
    plot.addLineStyle("-")
    plot.addLineStyle("--")
    plot.addLineStyle(":")
    
    #plot.hasLegend()
    
    # line = Line()
    # line.xValues = Xvalues
    # line.yValues = USvalues
    # line.label = "User time"
    # line.lineStyle = "-"
    # line.color = "red"
    # plot.add(line)
    # 
    # line = Line()
    # line.xValues = Xvalues
    # line.yValues = SYvalues
    # line.label = "System Time"
    # line.lineStyle = "--"
    # line.color = "blue"
    # plot.add(line)
    
    flows = (getFlows(lines))
    for k,v in flows[0].iteritems():
        fdesc = v.split('->')
        fsrc  = fdesc[0].split(':')
        fdst  = fdesc[1].split(':')
        fsrc  = '.'.join(fsrc[0].split('.')[2:4] + [fsrc[1]])
        fdst  = '.'.join(fdst[0].split('.')[2:4] + [fdst[1]])
        label = fsrc+'->'+fdst

        if sum(flows[2][k]) > 84778.0:
            line = Line()
            line.xValues = flows[1][k]
            line.yValues = flows[2][k]
            line.label = label
            print 'adding', label
            plot.add(line)
        else:
            print 'skipping', label
    
    # plot.setTwinX("Second Plot's Y Axis", 1)


    plot.save(sys.argv[3])
    
    return 0;

if __name__ == '__main__':
    try:
        parser = optparse.OptionParser(
            formatter=optparse.TitledHelpFormatter(),
            usage=globals()['__doc__'],
            version='')
        
        if len(sys.argv) < 4:
            print __doc__
            sys.exit()
        sys.exit(main())

    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        print 'Unexpected exception "%s"' % str(e)
        traceback.print_exc()
        sys.exit(1)
