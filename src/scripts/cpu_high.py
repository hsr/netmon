#!/usr/bin/env python
"""
SYNOPSIS
 
    Usage: %s <id> <input> <output>
 
DESCRIPTION
 
    Netmon database. Collects data from netmon clients and takes decisions about
    network allocation

"""


import sys
__doc__ = __doc__ % sys.argv[0]

import subprocess
import optparse
import traceback
import os

import time
import numpy as np
from threading import Thread
from datetime import datetime as dt


from boomslang import *

# third party code
script_dir = os.path.dirname(os.path.realpath(__file__));
sys.path.insert(0, '%s/third/' % script_dir);
from util import *

import nmparser

def main():
    global options
    
    lines             = nmparser.getFileLines(sys.argv[2])
    cpuLoad           = nmparser.getCPU(lines, relative=True)
    increase          = nmparser.getCPUIncrease(lines, cpuLoad[3],
                                                 relative=True)
    Xvalues, USvalues = cpuLoad[0], cpuLoad[1]
    SYvalues, X0      = cpuLoad[2], cpuLoad[3]

    #print increase
    
    plot  = Plot()
    
    plot.xLabel     = "Time (s)"
    plot.yLabel     = "CPU Increase Metric"
    plot.hasLegend()

    # scatter           = Scatter()
    # scatter.xValues   = increase[0]
    # half              = max(USvalues)*.5
    # # scatter.yValues = [half for i in xrange(len(increase))]
    # scatter.yValues   = increase[1]
    # scatter.label     = "Increase"
    # plot.add(scatter)
    
    line              = Line()
    line.xValues      = increase[0]
    # scatter.yValues = [half for i in xrange(len(increase))]
    line.lineStyle  = "-"
    line.color      = "blue"
    line.yValues      = increase[1]
    line.label        = "Increase"
    plot.add(line)


    line            = Line()
    line.xValues    = Xvalues
    line.yValues    = USvalues
    line.label      = "CPU Time"
    line.lineStyle  = "-"
    line.color      = "red"
    plot.add(line)

    plot.setTwinX("Measured CPU Time", 1)
    plot.setAxesLabelSize(5)

    layout = PlotLayout()
    layout.setPlotParameters(left=.09, right=.9, top=.95)

    layout.addPlot(plot)
    # #layout.legendLabelSize = 10
    layout.save(sys.argv[3])
    
    return 0;

if __name__ == '__main__':
    try:
        parser = optparse.OptionParser(
            formatter=optparse.TitledHelpFormatter(),
            usage=globals()['__doc__'],
            version='')
        
        if len(sys.argv) < 4:
            print __doc__
            sys.exit()
        sys.exit(main())

    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        print 'Unexpected exception "%s"' % str(e)
        traceback.print_exc()
        sys.exit(1)
