#!/bin/bash

. /home/hsr/.profile

alias pssh-vsocc='parallel-ssh -h ~/work/parallel-ssh/vsocc'
alias pssh-rsocc='parallel-ssh -h ~/work/parallel-ssh/rsocc'
alias pssh-socc='parallel-ssh -h ~/work/parallel-ssh/socc'

for CPUMEM in 30; do
	for RATE in 500 100 50 20; do
		for D in 3; do
			for I in 500; do
				for RUN in $(seq 0 4); do
					EXP_NAME=hadoop_r2_${1}_D_${D}_I_${I}_R_${RATE}_MEM_${CPUMEM}_RUN_${RUN}
					echo "Starting experiment "${EXP_NAME}

					echo "Removing old data from dfs..."
					ssh hadoop-head 'source /home/seelab/hadoop-util/scripts/config.sh; sudo -u hduser ${HADOOP_HOME}/hadoop dfs -rmr ${EXP_DIR}/${INPUT_DIR}-output'

					echo "Starting scheduler..."
					parallel-ssh -h ~/work/parallel-ssh/socc '/home/hsr/Code/ucsd/netmon/src/user/nm_scheduler.py --rate='${RATE}' &> /tmp/'${EXP_NAME}'_sched.txt &'

					echo "Killing all schedulers..."
					parallel-ssh -h ~/work/parallel-ssh/socc 'for K in $(ps aux | grep nm_scheduler | grep -v grep | awk "{print \$2}"); do echo "Killing ${K}..."; sudo kill -9 ${K}; done;'

					echo "Starting db..."
					sudo ../user/nm_database.py -v -i br0 --flow_search_depth=5000000 --flow_search_delay=${D} --inactivity_limit=${I}000 \
						--optimal --cpu_mem=${CPUMEM} --cpu_limit=.7 > /tmp/${EXP_NAME}_db.txt &
		
					echo "Skipping rand-flows"
					echo "Starting concurrent flows"
					parallel-ssh -h ~/work/parallel-ssh/rsocc '/root/netmon/src/user/rand-flow.py -t 4 -S random119-1,random125-1,random126-1,random131-1,random133-1,random134-1,random130-1,random132-1,random135-1,random136-1,random139-1,random138-1 -v --transfers=20000 &> '${EXP_NAME}'_rand.txt &'
		
					echo "Running job."
					(time ssh hadoop-head 'source /home/seelab/hadoop-util/scripts/config.sh; sudo -u hduser ${HADOOP_HOME}/hadoop jar /home/seelab/random-src/build.jar InvertedIndex ${EXP_DIR}/${INPUT_DIR} ${EXP_DIR}/${INPUT_DIR}-output &> /tmp/'${EXP_NAME}'_job.txt') 1> /tmp/${EXP_NAME}_job_out.txt 2>/tmp/${EXP_NAME}_job_time.txt

					echo "Killing database..."
					for K in $(ps aux | grep nm_database | grep -v grep | awk '{print $2}'); do 
						echo "Killing ${K}..."
						sudo kill -9 ${K}
					done;

					echo "Killing rand-flows"
					parallel-ssh -h ~/work/parallel-ssh/rsocc 'for K in $(ps aux | grep rand-flow | grep -v grep | awk "{print \$2}"); do echo "Killing ${K}..."; sudo kill -9 ${K}; done;'

					sleep 10;
				done
			done;
		done
	done
done