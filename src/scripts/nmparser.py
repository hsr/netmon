#!/usr/bin/env python
import sys


def getFileLines(filename = sys.argv[2]):
    f = open(filename,'r')
    lines = f.readlines()
    f.close()
    return lines

def getCPUIncrease(lines, timeReference = -1, relative=True):
    X = []
    Y = []
    X0 = timeReference
    for l in lines:
        if l[0:len('l(%s)' % sys.argv[1])] == ('l(%s)' % sys.argv[1]):
            # P = lines[i-1].split()
            c = l.split()
            # if float(c[6]) > -.7:
#                 continue
            # if (i+1 >= len(lines)):
            #     break;
            # N = lines[i+1].split()

            #print (c[1], c[2][3:], c[3][3:])

            X += [float(c[1])]
            if relative:
                X[-1] = [float(c[1])-X0]
            Y += [float(c[6])]
    return (X, Y)

def getCPUIncreaseTimes(lines, timeReference = -1, relative=True, limit=-.7):
    X = []
    Y = []
    X0 = timeReference
    for l in lines:
        if l[0:len('l(%s)' % sys.argv[1])] == ('l(%s)' % sys.argv[1]):
            # P = lines[i-1].split()
            c = l.split()
            # if float(c[6]) > -.7:
            #     continue
            # if (i+1 >= len(lines)):
            #     break;
            # N = lines[i+1].split()

            #print (c[1], c[2][3:], c[3][3:])
            if float(c[6]) > limit:
                continue
                
            X += [float(c[1])]
            if relative:
                X[-1] = [float(c[1])-X0]
            Y += [float(c[6])]
    return (X, Y)

def getCPU(lines, relative=True):
    X = []
    yus  = []
    ysy= []
    for l in lines:
        if l[0:len('c(%s)' % sys.argv[1])] == ('c(%s)' % sys.argv[1]):
            c = l.split()
            #print (c[1], c[2][3:], c[3][3:])
            X += [float(c[1])]
            yus += [float(c[2][3:])]
            ysy += [float(c[3][3:])]
    
    Xvalues = []
    USvalues = []
    SYvalues = []
    X0 = X[0]
    for i in xrange(1,len(X)):
        Xvalues += [X[i]]
        if relative:
            Xvalues[-1] -= X0

        USvalues += [yus[i] - yus[i-1]]
        SYvalues += [ysy[i] - ysy[i-1]]
    
    # for i in xrange(0,len(Xvalues)):
    #     print Xvalues[i], USvalues[i], SYvalues[i]
    
    #print Xvalues
    return (Xvalues, USvalues, SYvalues, X0)

def getFlows(lines, timeReference = -1, relative=True):
    FlowsDesc = {}
    FlowsX = {}
    FlowsY = {}
    X0 = timeReference
    for l in lines:
        if l[0:len('n(%s)' % sys.argv[1])] == ('n(%s)' % sys.argv[1]):
            c = l.split()
            #print (c[1], c[2][3:], c[3][3:])
            flow = c[2]+'->'+c[4]
            flow_hash = hash(flow)
            FlowsDesc[flow_hash]  = flow
            
            if X0 == -1:
                X0 = float(c[1])
            
            if not FlowsX.has_key(flow_hash):
                FlowsX[flow_hash] = []
            # elif float(c[1])-FlowsX[flow_hash][-1] > 3.:
            #     # The flow exists in the database, but was active for 
            #     # more than 2 seconds. Add a new point to identify
            #     # this "inactivity" graphically
            #     FlowsX[flow_hash] += [FlowsX[flow_hash][-1]+.0000001]
            #     FlowsY[flow_hash] += [0]
            FlowsX[flow_hash]    += [float(c[1])]
            
            if not FlowsY.has_key(flow_hash):
                FlowsY[flow_hash] = []
            FlowsY[flow_hash]    += [float(c[5][1:])]
    
    # Some flows only appear once in the input nm_database file, remove them
    # from FlowDesc
    for key in FlowsX.keys():
        if len(FlowsX[key]) < 2:
            del FlowsDesc[key]
            del FlowsX[key]
    
    X = {}
    Y = {}
    for flow_hash in FlowsX.keys():
        for i in xrange(1,len(FlowsX[flow_hash])):

            if not X.has_key(flow_hash):
                X[flow_hash] = []
            if relative:
                X[flow_hash] += [FlowsX[flow_hash][i]-X0]
            else:
                X[flow_hash] += [FlowsX[flow_hash][i]]

            if not Y.has_key(flow_hash):
                Y[flow_hash] = []
            Y[flow_hash] += [FlowsY[flow_hash][i]-FlowsY[flow_hash][i-1]]
            if Y[flow_hash][-1] < 0:
                Y[flow_hash][-1] = 0;
    
    for key in FlowsDesc.keys():
        if len(X[key]):
            X[key] = [X[key][0]+.000001]+X[key]
            Y[key] = [0]+Y[key]
    
    return (FlowsDesc, X, Y)

def getDetectedFlows(lines):
    FlowsDesc = {}
    for l in lines:
        if '!!' in l:
            c = l.split()
            #print (c[1], c[2][3:], c[3][3:])
            flow = c[2]+'->'+c[4]
            flow_hash = hash(flow)
            FlowsDesc[flow_hash]  = flow
    
    return (FlowsDesc)