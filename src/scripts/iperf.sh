#!/bin/bash

source /home/hsr/.profile

for D in 3 4 5; do
	for I in 500; do
		echo "Starting db..."
		sudo ../user/nm_database.py -v -i br0 --flow_search_depth=5000000 --flow_search_delay=${D} --inactivity_limit=${I}000 \
			 > /tmp/iperf_D_${D}_I_${I}_new.txt &
		echo "Running benchmark on 137..."
		ssh dcswitch137 'for TIME in 10 25 15; do iperf -c 192.168.125.254 -p 4041 -t 20 -i 1 & iperf -c 192.168.125.254 -p 4042 -t 10 -i 1 & iperf -c 192.168.125.254 -p 4043 -t 4 -i 1; ssh 192.168.125.254 "sysbench --test=cpu --num-threads=8 --cpu-max-prime=20000 run"; sleep ${TIME}; done';
		echo "Killing all..."
		for K in $(ps aux | grep nm_database.py | grep -v grep | awk '{print $2}'); do 
			echo "Killing ${K}..."
			sudo kill -9 ${K};
		done;
		echo "Killing all netperfs..."
		ssh dcswitch137 'pkill iperf'
		sleep 10;
	done;
done