import sys
import subprocess
from threading import Thread

def abort(msg):
    print msg
    sys.stdout.flush()
    sys.exit(1)

def warn(msg):
    print "WARNING:", msg
    sys.stdout.flush()
    
def run(cmd, bg=False, checkReturn=True, verbose=False):
    if verbose:
        print "cmd:", cmd
        sys.stdout.flush()
    try:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, 
                             stderr=subprocess.PIPE, shell=True,
                             executable='/bin/bash')
        if bg:
            return
        output,stderr = p.communicate()
        if checkReturn and p.returncode:
            abort('unexpected return code %d for "%s".\n%s\n%s' % 
                  (p.returncode, cmd, output, stderr))        
        return output.strip()
    except Exception, e:
        None
        

def ssh(machine, cmd, user = 'root', bg=True, checkReturn=True, verbose=False):
    return run("ssh -o ConnectTimeout=3 %s@%s '%s'" % (user, machine, cmd), 
               bg=bg, checkReturn=checkReturn, verbose=verbose);

# Run the cmd given on each of the machines in parallel. 
# Note: This function never runs ssh in background.
#       This function will block until all the sshs return
def pssh(machines, cmd, user = 'root', checkReturn=True, verbose=False):
    threads = [];

    for m in machines:
        threads += [Thread(target=ssh,
                           args=([m,cmd,user,False,checkReturn,verbose]))]

    # TODO: Threads have a sys return code, check it!
    map(lambda t: t.start(), threads)
    try:
        map(lambda t: t.join(60), threads) # It shouldn't take more than 1 min
    except KeyboardInterrupt: # Ctrl-C
        print "Thread wait interrupted"
    except EnvironmentError:
        print "Signal received/Env error"
    except:
        print "An error occurred!"
    sys.stdout.flush()

    for t in threads:
        if t.isAlive():
            warn("pssh: a thread didn't finish!")
            return 0
    return -1

def scp(machine, localfile, remotepath, user = 'root'):
    return run("scp -o ConnectTimeout=3 %s %s@%s:%s" % \
                   (localfile, user, machine, remotepath))
