#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#include <pthread.h>

#include <netinet/ip.h>
#include <net/if.h>
#include <net/ethernet.h>
#include <linux/if_ether.h>   // ETH_P_ARP = 0x0806, ETH_P_ALL = 0x0003

#include "uthash.h"
#include "netmon.h"
#include "util.h"
#include "net_util.h"
#include "nm_translator.h"

#define SAFE_HASH_FIND_INT(rwlock,hash,id,element) {\
	if (pthread_rwlock_rdlock(&rwlock) != 0) eexit("can't get rdlock");\
	HASH_FIND_INT(hash, id, element);\
	pthread_rwlock_unlock(&rwlock);}

#define SAFE_HASH_ADD_INT(rwlock,hash,id,element) {\
	if (pthread_rwlock_wrlock(&rwlock) != 0) eexit("can't get rdlock");\
	HASH_ADD_INT(hash, id, element);\
	pthread_rwlock_unlock(&rwlock);}

/* TODO: create locks withing the datastructre instead of global locks */
#define SAFE_HASH_GET_INT(rwlock,hash,id,element) {\
	if (pthread_rwlock_rdlock(&rwlock) != 0) eexit("can't get rdlock");\
	HASH_FIND_INT(hash, id, element);}
	
#define SAFE_HASH_PUT_INT(rwlock) {\
	pthread_rwlock_unlock(&rwlock);}
	
#define SAFE_HASH_FIND_INT(rwlock,hash,id,element) {\
	if (pthread_rwlock_rdlock(&rwlock) != 0) eexit("can't get rdlock");\
	HASH_FIND_INT(hash, id, element);\
	pthread_rwlock_unlock(&rwlock);}




#define HISTORY_SIZE 10
struct cpu_act {
	u32      id;
	uint64_t act[HISTORY_SIZE];
	uint64_t act_total[HISTORY_SIZE];
	u32      act_ptr;
	UT_hash_handle hh;
};


struct cpu_act *cpu_hash;
pthread_rwlock_t cpu_hash_l;

typedef struct  {
    struct {
  		__be32 src;	/* IP source address. */
  		__be32 dst;	/* IP destination address. */
    } addr;
    struct {
  	__be16 src;		/* TCP/UDP source port. */
  	__be16 dst;		/* TCP/UDP destination port. */
    } tp;	
} flow_act_key;

struct flow_act {
	flow_act_key key;
	uint64_t act[HISTORY_SIZE];
	UT_hash_handle hh;
};

struct database *nmdb;

inline void *
nmdb_byte_ptr(int64_t int_ptr) {
	return ((void*) nmdb->m) + int_ptr;
}

void
nm_print_netmonmsg(struct netmonmsg *nms) {
	printf("%d.%d ", (int)nms->ts.tv_sec, (int)nms->ts.tv_usec);
	
	if (nms->s_type == S_NET) {
		printf("net_stats (%"PRIu64")\t"
			""IP_FMT" -> "IP_FMT"\t"
			"by:%-10"PRIu64
			"pk:%-10"PRIu64
			"\n",
		nms->sender,
		IP_ARGS(&(nms->st[0].net[0].addr.src)),
		IP_ARGS(&(nms->st[0].net[0].addr.dst)),
		nms->st[0].net[0].bytes,
		nms->st[0].net[0].packets);
	}
	else if (nms->s_type == S_CPU) {
		printf("cpu_stats (%"PRIu64
			")\t"
			"us:%-10"PRIu64
			"sy:%-10"PRIu64
			"id:%-10"PRIu64
			"wa:%-10"PRIu64
			"hi:%-10"PRIu64
			"st:%-10"PRIu64
			"\n",
			nms->sender,
			nms->st[0].cpu[0].t_us,nms->st[0].cpu[0].t_sy,
			nms->st[0].cpu[0].t_id,nms->st[0].cpu[0].t_wa,
			nms->st[0].cpu[0].t_hi,nms->st[0].cpu[0].t_st);
	}
}

/**
 * Given a pointer to the beginning of a stats msg on the database,
 * return a pointer to the previous message.
 **/
/* inline struct netmonmsg * nmdb_previous(int64_t ptr) {
	int msgsize = *((int*)&nmdb->m[ptr-sizeof(int)]);
	return (struct netmonmsg *) &nmdb->m[ptr-sizeof(int)-msgsize];
} */

inline struct netmonmsg * nmdb_netmonmsg(int64_t ptr) {
	return (struct netmonmsg *) nmdb_byte_ptr(ptr);
}

inline int nmdb_previous(int64_t ptr) {
	int msg_size;
	
	if (ptr <= (sizeof(struct netmonmsg) + sizeof(int)))
		return -1;

	msg_size = *((int*) nmdb_byte_ptr(ptr - sizeof(int)));
	return ptr - sizeof(int) - msg_size;
}

/* Assume m2 was received after m1 */
inline int timediff(struct netmonmsg *m1, struct netmonmsg *m2) {
	int sec  = (int) m2->ts.tv_sec  - m1->ts.tv_sec;
	int usec = (int) m2->ts.tv_usec - m1->ts.tv_usec;
	return usec + sec*1e6;
}

int64_t nmdb_previous_net(u32 id, int64_t ptr) {
	struct netmonmsg *nms;
	
	if ((ptr = nmdb_previous(ptr)) < 0)
		return ptr;
	nms  = nmdb_netmonmsg(ptr);

	while (nms->s_type != S_NET || nms->sender != id) {
		if ((ptr = nmdb_previous(ptr)) < 0)
			return ptr;
		nms = nmdb_netmonmsg(ptr);
	}
	return ptr;
}

/**
 * Returns a list of flows that might have contributed to increase in cpu
 * activity
 **/
void find_cpu_flows (u32 id) {
	/* Look for flows which have finished in the last 2sec */
	int ptr = nmdb->head;
	struct netmonmsg *nms;
	struct netmonmsg *nms_prev;
	
	if ((ptr = nmdb_previous_net(id, ptr)) < 0) { return; }
	nms_prev = nmdb_netmonmsg(ptr);
	
	if ((ptr = nmdb_previous_net(id, ptr)) < 0) { return; }
	nms = nmdb_netmonmsg(ptr);

	while (timediff(nms, nms_prev) < 2*1e6) {	
		/* if (nms_prev->s_type == S_CPU) */
		printf("%d\t", ptr);
		nm_print_netmonmsg(nms_prev);
		
		nms_prev = nms;
		if ((ptr = nmdb_previous_net(id, ptr)) < 0) { return; }
		nms = nmdb_netmonmsg(ptr);
	}
}

inline int
circ_prev(int ptr) {
	if (ptr == 0)
		return HISTORY_SIZE - 1;
	return ptr - 1;
}

#define WIN_SIZE  2
int
cpu_winsize_increase_fn(struct cpu_act *ca) {
	double slope = 0;
	u32 prev, curr;
	u64 act_curr, act_prev;
	u32 i;
	
	curr = ca->act_ptr;
	prev = circ_prev(ca->act_ptr);
	
	act_curr = 0;
	for (i = WIN_SIZE; i > 0; i--) {
		act_curr += (ca->act[curr] - ca->act[prev]);
		curr = prev;
		prev = circ_prev(prev);
	}
	
	act_prev = 0;
	for (i = WIN_SIZE; i > 0; i--) {
		act_prev += (ca->act[curr] - ca->act[prev]);
		curr = prev;
		prev = circ_prev(prev);
	}
	
	slope = ((double) act_curr) /
			((double) act_prev);
	
	printf("(%d) %f\t%llu\t%llu\n",  ca->id, slope, act_curr, act_prev);
	
	if (act_prev > 0 && act_curr > 0 && slope > 8) {
		printf("(%d) possible increase in cpu usage! Slope is %f\n", ca->id, slope);
		/* printf("%f\t%llu\t%llu\n",  slope, act_curr, act_prev); */
		return 1;
	}
	return 0;

}

int
cpu_increase_fn(struct cpu_act *ca) {
	double slope;
	u32 prev, curr;
	u64 act_curr, act_prev;
	
	curr = ca->act_ptr;
	prev = circ_prev(ca->act_ptr);
	
	act_curr = ca->act[curr] - ca->act[prev];
	act_prev = ca->act[prev] - ca->act[circ_prev(prev)];
	
	/* slope = (double)(ca->act[curr] - ca->act[prev])/
			(double)(ca->act_total[curr] - ca->act_total[prev]);
	 */
	slope = ((double) act_curr) /
			((double) act_prev);
	
	if (act_prev > 0 && act_curr > 0 && slope > 4) {
		printf("Possible increase in cpu usage! Slope is %f\n", slope);
		printf("%f\t%llu\t%llu\n",  slope, act_curr, act_prev);
		return 1;
	}
	printf("%f\t%llu\t%llu\n",  slope, act_curr, act_prev);
	return 0;
}

void
process_cpu_s(u8 *data) {
	struct netmonmsg *nms = (struct netmonmsg *) &data[14];
	struct cpu_stats *cpu_s = &nms->st[0].cpu[0];
	struct cpu_act *ca;
	
	if (nms->s_type != S_CPU)
		return;
	
	/* TODO: replace with get/put */
	SAFE_HASH_FIND_INT(cpu_hash_l, cpu_hash, &nms->sender, ca);
	if (!ca) {
        ca = (struct cpu_act *)malloc(sizeof(struct cpu_act));
		memset(ca, 0, sizeof(struct cpu_act));
        ca->id = nms->sender;
        SAFE_HASH_ADD_INT(cpu_hash_l, cpu_hash, id, ca);
	}
	ca->act_ptr = (ca->act_ptr+1) % HISTORY_SIZE;
	ca->act[ca->act_ptr]       = cpu_s->t_us + 
						         cpu_s->t_sy;

	ca->act_total[ca->act_ptr] = cpu_s->t_us +
								 cpu_s->t_sy + 
								 cpu_s->t_id +
								 cpu_s->t_wa + 
								 cpu_s->t_hi +
								 cpu_s->t_st;
	
	printf("RE: %"PRIu64" %"PRIu64" %"PRIu64" %"PRIu64" %"PRIu64" %"PRIu64"\n",
		cpu_s->t_us,
		cpu_s->t_sy,
		cpu_s->t_id,
		cpu_s->t_wa,
		cpu_s->t_hi,
		cpu_s->t_st);
	
	if (cpu_winsize_increase_fn(ca)) {
		find_cpu_flows(ca->id);	
	}
}

void
nm_print_cpu_s(u8 *data) {
	struct netmonmsg *nms = (struct netmonmsg *) &data[14];
	
	if (nms->s_type != S_CPU) { return; }
	nu_print_eth(data);
	nm_print_netmonmsg(nms);	
}

void
nm_print_net_s(u8 *data) {
	struct netmonmsg *nms = (struct netmonmsg *) &data[14];

	if (nms->s_type != S_NET) { return; }
	nu_print_eth(data);
	nm_print_netmonmsg(nms);
}

/* 
 This is the structure of the msg buffer:
 +------+-----------+------+-----------+---+----------+-----+---------------+
 | msg1 | size msg1 | msg2 | size msg2 |...| last msg | pad | size last msg |
 +------+-----------+------+-----------+---+----------+-----+---------------+

 The size of messages allow searching for previous messages with different 
 sizes

*/

/**
 * This function implements the circular buffer logic for nmdb
 * It returns the position in the allocated memory buffer
 * that the caller should memcpy the previous block size. 
 * If we are advancing nmdb->head past buffer space, then the caller 
 * should write the size of the last block at the end of the buffer.
 **/

inline void *
nmdb_step(struct netmonmsg *nms) {
	void * next = NULL;
	int size = -1;
	
	if (nms->s_type == S_NET)
		size = sizeof(struct netmonmsg)+sizeof(struct net_stats);
	else if (nms->s_type == S_CPU)
		size = sizeof(struct netmonmsg)+sizeof(struct cpu_stats);

	if (nmdb->head + size + sizeof(int) > nmdb->total) {
		nmdb->head = 0;
	}
	else
		nmdb->head += size + sizeof(int);
	return next;
}

/* Add msg to the database. Return the amount of bytes consumed from the
database */
int
nmdb_add(u8 *data) {
	struct netmonmsg *nms = (struct netmonmsg *) &data[14];
	int size = -1;
	
	if (nms->s_type == S_NET)
		size = sizeof(struct netmonmsg)+sizeof(struct net_stats);
	else if (nms->s_type == S_CPU)
		size = sizeof(struct netmonmsg)+sizeof(struct cpu_stats);

	if (nmdb->head + size + sizeof(int) > nmdb->total)
		return -1;
	
	/* printf("trying to write to %llu, %d bytes. Alloc is %llu\n",
			(nmdb->head), (int)size + sizeof(int), nmdb->total); */
			
	if (size > 0) {
		memcpy(nmdb_byte_ptr(nmdb->head), nms, size);
		memcpy(nmdb_byte_ptr(nmdb->head+size), &size, sizeof(int));
		nmdb->head += size + sizeof(int);
		return size+sizeof(int);
	}
	return size;
}

void
nm_listen(char *iface, uint total) {	
	u8 frame[ETH_FRAME_LEN];
	int s, bytes;

	if ((s = socket(AF_PACKET, SOCK_RAW, ntohs(ETH_P_NETMON))) < 0)
		eexit("nm_listen: error opening socket");

	printf("test\n");
	/* TODO: figure out why the following code isn't working */
	
	/* if (setsockopt(s, SOL_SOCKET, SO_BINDTODEVICE, iface, strlen(iface)) < 0)
		eexit("nm_listen: error binding to iface"); */
	
	while (total > 0) {
		bytes = recv(s, frame, ETH_FRAME_LEN, 0);

		if (bytes < 0 && errno != EINTR)
			eexit("Error receiving frame");
		
		nm_print_cpu_s((u8*)&frame);
		nm_print_net_s((u8*)&frame);

		if ((bytes = nmdb_add((u8*)&frame)) < 0)
			break;
			/* eexit("err: adding to database"); */
		
		process_cpu_s((u8*)&frame);
		
		total -= bytes;
		
		/* printf("recv %d bytes\n", bytes); */
	}
	printf("db full\n");
	close(s);
}

int
nmdb_malloc(int size) {
	nmdb = (struct database *) malloc (
			sizeof(struct database) + size);

	if (nmdb) {
		nmdb->head  = 0;
		nmdb->total = size;
		printf("allocating size %d\n", size);
		return size;
	}
	return -1;
}

int nmdb_free() { free(nmdb); return 0; }
void usage() { eexit("Usage: nm_database <iface> <size> <output_file>"); }	

int 
main(int argc, char *argv[]) {
	int total;
	/* pthread_t t; */

	if (argc < 4)
		usage();
	
	/* Initialization */
	cpu_hash = NULL;
	if (pthread_rwlock_init(&cpu_hash_l,NULL)!=0) eexit("can't create rwlock");
	
	if ((total = nmdb_malloc(atoi(argv[2]))) < 0)
		eexit("Error allocating memory for the database");

	/* create thread to listen & reply nmquerymsgs (translate) */
	/* if (pthread_create(&t, NULL, &nm_translator, (void *)argv[1]) < 0)
		eexit("could not create thread"); */

	nm_listen(argv[1], total);

	/* TODO: write DB to a file */
	
	/* Listen for packets with stats */
	nmdb_free();

	return 0;
}
