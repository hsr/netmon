#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "util.h"

void eexit(char *msg) { perror(msg); exit(1); }

void warn(char *msg) { printf("WARNING: %s\n", msg); }
