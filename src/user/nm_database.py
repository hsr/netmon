#!/usr/bin/env python
 
import sys

"""
SYNOPSIS
 
    Usage: %s [options]
 
DESCRIPTION
 
    Netmon database. Collects data from netmon clients and takes decisions
    about network allocation

""" % sys.argv[0]

import subprocess
import os
import optparse
import traceback

import struct

#import time
#import numpy as np
#from threading import Thread
from datetime import datetime as dt
import socket as sk

# third party code
script_dir = os.path.dirname(os.path.realpath(__file__));
sys.path.insert(0, '%s/../scripts/third/' % script_dir);
from util import *

ETH_P_NETMON = 0xf370
CPU_STAT     = 1
NET_STAT     = 2


NETMON_SCHEDULER_PORT = 8989

global options

class NetmonStat(object):
    """NetmonStat"""
    def __init__(self, timeval, sender):
        self.timeval = timeval
        self.sender  = sender
        return;

    @staticmethod
    def parseMessage(data):
        #nmmsgfmt = 'llllIII'
        # struct netmonmsg {
        #   struct timeval  ts;           /* Timestamp */
        #   int64_t         sender;
        #   unsigned short  s_type;       /* CPU or NET stats */
        #   struct nm_stats st[0];        /* Stats */
        # };
        netmonmsg_fmt  = 'llQH'
        if (len(data) < 14 + struct.calcsize(netmonmsg_fmt)):
            raise Exception;

        netmonmsg_hdr  = data[14:14+struct.calcsize(netmonmsg_fmt)]
        netmonmsg_data = data[14+struct.calcsize(netmonmsg_fmt)+1:]
        try:
            msg = struct.unpack(netmonmsg_fmt, netmonmsg_hdr)
        except Exception, e:
            raise Exception
        if len(msg) == 4:
            return (msg[0],msg[1]), msg[2], msg[3], netmonmsg_data
        raise Exception;
        
    def getType(self):
        raise NotImplemented;
        
    def getTime(self):
        return self.timeval;

    def getSender(self):
        return self.sender;

    def __str__(self):
        args = (self.sender,)+self.timeval
        return '(%d) %d.%d ' % args

def ip_addr_args(ip = 0):
    return tuple(map(lambda n: int(ip>>n & 0xFF), [24,16,8,0]))

class NetmonNetStat(NetmonStat):
    def __init__(self, timeval, sender, data):
        super(NetmonNetStat, self).__init__(timeval, sender)
        msg_fmt    = '>xxxxxLLHHQQ'
        msg_data   = data[:struct.calcsize(msg_fmt)]

        p   = struct.unpack(msg_fmt, msg_data)
        self.addr,self.tp = ((p[0],p[1]),(p[2],p[3]))
        self.bytes = sk.ntohl(p[4])
        self.pkts  = sk.ntohl(p[5])

    def __str__(self):
        s = super(NetmonNetStat, self).__str__()
        args = ip_addr_args(self.addr[0])+(self.tp[0],)+ \
               ip_addr_args(self.addr[1])+(self.tp[1],)+ \
               (self.bytes,self.pkts)
        return 'n' + s + '%d.%d.%d.%d:%d -> %d.%d.%d.%d:%d (%d B,%d P)' % args
        
    def getType(self):
        return NET_STAT;
        
class NetmonCPUStat(NetmonStat):
    def __init__(self, timeval, sender, data):
        super(NetmonCPUStat, self).__init__(timeval, sender)
        msg_fmt = '<xxxxxQQQQQQ' # WHY SO MUCH PADDING?
        msg_data = data[:struct.calcsize(msg_fmt)]
        self.t_us,self.t_sy,self.t_id,self.t_wa,self.t_hi,self.t_st = \
                struct.unpack(msg_fmt, msg_data)

    def getType(self):
        return CPU_STAT;

    def __str__(self):
        s = super(NetmonCPUStat, self).__str__()
        args = self.t_us,self.t_sy,self.t_id,self.t_wa,self.t_hi,self.t_st
        return 'c' + s + 'us:%d sy:%d id:%d wa:%d hi:%d st:%d' % args

class CPUMonitor(dict):
    """docstring for CPUMonitor"""
    def __init__(self, memorySize):
        super(CPUMonitor, self).__init__()
        self.memorySize = memorySize
        self.delay      = 0

    @staticmethod
    def linreg(X, Y):
        """ return a,b in solution to y = ax + b such that root mean square
        distance between trend line and original points is minimized """
        N = len(X)
        Sx = Sy = Sxx = Syy = Sxy = 0.0
        for x, y in map(None, X, Y):
            Sx = Sx + x
            Sy = Sy + y
            Sxx = Sxx + x*x
            Syy = Syy + y*y
            Sxy = Sxy + x*y
        det = Sxx * N - Sx * Sx
        return (Sxy * N - Sy * Sx)/det, (Sxx * Sy - Sx * Sxy)/det

    def cpu_increase_linreg(self,l, sender, msg):
        global options
        avg   = sum(l)/len(l)
        predY = CPUMonitor.linreg(range(len(l)),l)

        if not predY[1]:
            return 0;
        
        metric = -1*(1/(avg/predY[1]))
        if options.verbose:
            print "l(%d) %d.%d %-10d %-10d %-10d %-10f %-10f" % \
                (sender, msg.timeval[0], msg.timeval[1], avg, 
                 predY[1], avg-predY[1], avg/predY[1], 1/(avg/predY[1]))
        if metric > float(options.cpu_limit):
            return 1
        return 0

    def cpu_increase_fn(self, sender, msg):
        if not self.has_key(sender):
            raise Exception
        if len(self[sender]) < self.memorySize:
            return 0
        
        cpu_us = []
        prev = self[sender][0].t_us# + self[sender][0].t_sy
        for i in xrange(1,len(self[sender])):
            cur = self[sender][i].t_us# + self[sender][i].t_sy
            cpu_us.append(cur-prev)
            prev = cur

        if self.cpu_increase_linreg(cpu_us, sender, msg):
            return 1;
        return 0;

    def check(self, cpuMessage):
        """cpuMessage need to be an object of type NetmonCPUStat"""
        global options
        self.delay -= 1;
        if self.delay == 1:
            return 1;

        sender = cpuMessage.getSender()
        if self.has_key(sender):
            self[sender] += [cpuMessage]
        else:
            self[sender] = [cpuMessage]
        while len(self[sender]) > self.memorySize:
            self[sender].pop(0)
        if self.cpu_increase_fn(sender, cpuMessage):
            self.delay = int(options.flow_search_delay);
        return 0;

    def __str__(self):
        s = ''
        for l in self.values():
            print len(l)
            for v in l:
                s += '%s\n' % str(v)
        return s

def timeval2us(tv):
    return (tv[0]*1e6)+tv[1]

def timevaldelta(t1, t2):
    #print t1, t2
    
    us = t1[1]-t2[1]
    if us < 0:
        us *= -1;
        s = t1[0]-t2[0]-1
    else:
        s = t1[0]-t2[0]
    return (s,us)

class Flow(dict):
    """docstring for Flow"""
    def __init__(self, id, bytes = 0, time=(0,0)):
        super(Flow, self).__init__()
        self.id   = id;
        self.time = time;
        self.hist = [bytes] if bytes else [];
    def append(self, bytes):
        "Flow history goes from more recent to older"
        self.hist.append(bytes)
    def __len__(self):
        return len(self.hist);

    def serialize(self):
        ips   = self.id[0]
        ports = self.id[1]
        args  = ip_addr_args(ips[0])+(ports[0],)+ \
                ip_addr_args(ips[1])+(ports[1],)
        id    = '%d.%d.%d.%d:%d-%d.%d.%d.%d:%d ' % args
        return id

    def __str__(self):

        ips   = self.id[0]
        ports = self.id[1]
        args  = ip_addr_args(ips[0])+(ports[0],)+ \
                ip_addr_args(ips[1])+(ports[1],)
        id    = '%d.%d.%d.%d:%d -> %d.%d.%d.%d:%d ' % args
        hist  = ','.join(['%s' % str(b) for b in self.hist])
        time  = '%d.%d ' % (self.time[0], self.time[1])
        return id+time+hist

    def inactive(self, now):
        global options
        #delta = (now[0]-self.time[0])*1e6+(now[1]-self.time[1])
        delta = timeval2us(timevaldelta(now, self.time))
        #print delta, int(options.inactivity_limit)
        if delta > int(options.inactivity_limit):
            if options.optimal and self.id[1][1] == 5252:
                print '!!skipping flow with dst port 5252'
                return 0
            print '!!delta(%d)' % delta,
            return 1
        if len(self.hist) < 2:
            return 0
        if (self.hist[0]-self.hist[1]) < int(options.flow_size_limit):
            if options.optimal and self.id[1][1] == 5252:
                print '!!skipping flow with dst port 5252'
                return 0
            print '!!size(%d)' % (self.hist[0]-self.hist[1]),
            return 2
        return 0

class NetmonDatabase:
    """Stores all information received by the controller"""
    def __init__(self):
        global options
        self.head   = 0;
        self.stats  = [];
        self.cpumon = CPUMonitor(int(options.cpu_mem))
        self.sconn  = {}

    def findFlows(self, sender, history = 10):
        """Look for flows that could contribute to CPU increase on sender"""
        #print "looking for flows contributing to %d" % sender
        flows  = {}
        total  = len(self.stats)-1
        now    = self.stats[total].timeval
        for i in xrange(0,len(self.stats)):
            s     = self.stats[total-i]
            delta = timeval2us(timevaldelta(now,s.timeval))
            if delta > int(options.flow_search_depth):
                break;

            if s.sender == sender and s.getType() == NET_STAT:
                key = hash(s.addr+s.tp)
                if not flows.has_key(key):
                    flows[key] = Flow(id=(s.addr,s.tp),
                                      bytes=s.bytes,
                                      time=s.timeval)
                elif len(flows[key]) < history:
                    flows[key].append(s.bytes)
        print 'found:'
        highFlows = []
        for f in flows.values():
            inactive = f.inactive(now)
            if inactive:
                print '!!(%d)' % inactive,
            print '  ', str(f)
            highFlows.append(f.serialize())
        return highFlows

    def notify(self, flows):
        """notify schedulers at senders about important flows"""
        for f in flows:
            src = f.split(':')[0]
            if not self.sconn.has_key(src):
                try:
                    s = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
                    s.connect((src, NETMON_SCHEDULER_PORT))
                    self.sconn[src] = s;
                except Exception, e:
                    print "Could not connect to %s" % src
            else:
                try:
                    s.send(f)
                except Exception, e:
                    print "Could not send %s to %s" % (f,src)

    def add(self, data):
        """Parse data and add information to the netmon database"""
        global options
        timeval, sender, msgType, msgData = NetmonStat.parseMessage(data)
        s = None
        if (msgType == CPU_STAT):
            s = NetmonCPUStat(timeval, sender, msgData)
            self.stats.append(s)
            if (self.cpumon.check(s)):
                self.notify(self.findFlows(s.getSender()))
        if (msgType == NET_STAT):
            s = NetmonNetStat(timeval, sender, msgData)
            self.stats.append(NetmonNetStat(timeval, sender, msgData))

        if options.verbose:
            if s:
                print s
                sys.stdout.flush()

        if int(options.nmdb_size):
            while len(self.stats) > int(options.nmdb_size):
                warn('nmdb full, deleting old entries')
                self.stats.pop(0)

def nm_listen(iface='eth2'):
    nmdb = NetmonDatabase()
    
    s = sk.socket(sk.AF_PACKET, sk.SOCK_RAW, sk.htons(ETH_P_NETMON))
    s.bind((iface, sk.htons(0)))
    
    while True:
        data, client = s.recvfrom(65565)
        nmdb.add(data)

def main():
    global options
    
    print "Starting netmon database"
    print "Inactivity limit (-A) set to %d us" % int(options.inactivity_limit)
    print "Search depth (-D) set to %d us" % int(options.flow_search_depth)
    print "Search delay (-F) set to %d msgs" % int(options.flow_search_delay)
    print "Flow size limit (-S) set to %d B" % int(options.flow_size_limit)
    
    nm_listen(options.iface)
    
    return 0;

if __name__ == '__main__':
    try:
        parser = optparse.OptionParser(
            formatter=optparse.TitledHelpFormatter(),
            usage=globals()['__doc__'],
            version='')
        
        parser.add_option('-v', '--verbose', action='store_true',
                          default=False, help='verbose output')

        parser.add_option('-c', '--cpu_mem', action='store',
                          default=10,
                          help='Memory of CPU Monitor (default=10)')

        parser.add_option('-D', '--flow_search_depth', action='store',
                          default=1e6,
                          help='Ignore reports older than -D (us default:1e6)')

        parser.add_option('-F', '--flow_search_delay', action='store',
                          default=4,
                          help='CPU msgs recvd before flow search (default:5)')

        parser.add_option('-A', '--inactivity_limit', action='store',
                          default=1e6,
                          help='Flow assumed inactive after -Aus (default:1e6)')

        parser.add_option('-S', '--flow_size_limit', action='store',
                          default=1e3,
                          help='Flows < -S B assumed inactive (default:1e3)')

        parser.add_option('-C', '--cpu_limit', action='store',
                          default=.7,
                          help='Lin. reg. limit to assume CPU inc (default:.7)')

        parser.add_option('-N', '--nmdb_size', action='store',
                          default=0,
                          help='Max size of netmon db (default inf messages)')

        parser.add_option('-O', '--optimal', action='store_true',
                          default=False,
                          help='Do not try to limit flows with dst 5252')

        parser.add_option('-i', '--iface', action='store',
                          default='eth2', 
                          help='Interface to capture packets')


                        
        (options, args) = parser.parse_args(args=sys.argv)
        start = dt.now()
        sys.exit(main())

    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        print 'Unexpected exception "%s"' % str(e)
        traceback.print_exc()
        sys.exit(1)
