#!/usr/bin/env python
 
import sys

"""
SYNOPSIS
 
    Usage: %s [options]
 
DESCRIPTION
 
    Netmon scheduler. Listens to netmon port for information about important
    flows and controls bandwidth allocation

""" % sys.argv[0]

import subprocess
import os
import optparse
import traceback

import struct

#import time
#import numpy as np
#from threading import Thread
from datetime import datetime as dt
import socket as sk

# third party code
script_dir = os.path.dirname(os.path.realpath(__file__));
sys.path.insert(0, '%s/../scripts/third/' % script_dir);
from util import *

import select

NETMON_SCHEDULER_PORT = 8989

LEASE = 40

global options


# Rates are in mbps
def configureRates(nic='eth2', rate=-1, ceil=-1):
    global options

    run("sudo tc qdisc del dev %s root" % nic, checkReturn=False)
    
    if rate > 0:
        if ceil < 0:
            ceil = rate;

        run('sudo tc qdisc add dev %s root handle 1: htb r2q 1000 default 10'%\
            (nic))

        run('sudo tc class add dev %s parent 1: classid 1:1 \
             htb rate %dmbit ceil %dmbit burst 200kb cburst 200kb' % \
             (nic, rate, ceil))

        run('sudo tc class add  dev %s parent 1:1 classid 1:10 \
            htb rate %dmbit ceil %dmbit burst 200kb cburst 200kb' % \
            (nic,rate*(1.-float(options.high)),ceil))

        run('sudo tc class add  dev %s parent 1:1 classid 1:20 \
            htb rate %dmbit ceil %dmbit burst 200kb cburst 200kb' % \
            (nic,rate*(float(options.high)),ceil))

        run('sudo tc qdisc add dev %s parent 1:10 pfifo limit 50' % \
            nic)

        run('sudo tc qdisc add dev %s parent 1:20 pfifo limit 50' % \
            nic)

        #sudo tc qdisc add dev ${iface} parent 1:10 red limit 75000 

        # run('sudo tc filter add dev %s parent 1: proto all prio 1 \
        #     u32 match u32 0 0 flowid 1:10' % nic)

def assignFlow(nic='eth2', id=-1, dst=''):
    if not len(dst):
        print 'blank dst'
        return
    if id < 0:
        print 'id needed!'
        return

    run('sudo tc filter add dev %s parent 1: proto all prio 1 \
        handle 800::%d u32 match ip dst %s/32 flowid 1:20' % 
        (nic,id,dst),checkReturn=False) 

def removeFlow(nic='eth2', id=-1):
    if id < 0:
        print 'id needed!'
        return
    run('sudo tc filter del dev %s parent 1: proto all prio 1 \
        handle 800::%d u32' % 
        (nic,id),checkReturn=False) 

def nm_listen(iface='eth2'):
    cntr = 0
    lease = int(options.lease)
    configureRates(nic=str(options.iface),rate=int(options.rate))
    if int(options.rate) < 0:
        return
    prio = {}
    
    try:
        serverSocket = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
        serverSocket.bind(('', NETMON_SCHEDULER_PORT))
        serverSocket.listen(5)
        socketList = [serverSocket]
        
        while True:
            skin, skout, skex = select.select(socketList, [], [], 1)

            for s in skin:
                if s == serverSocket:
                    # handle the server socket 
                    clientSocket, clientAddress = s.accept()
                    print "Connected to ", str(clientAddress)
                    socketList.append(clientSocket)
                else: 
                    # handle all other sockets
                    data = s.recv(1024)
                    if data:
                        print data
                        flows = data.split(';')
                        for f in flows:
                            dst = data.split('-')[1]
                            dst_ip = dst.split(':')[0]
                            if not prio.has_key(dst_ip):
                                prio[dst_ip] = {
                                    'id': cntr,
                                    'lease': lease,
                                    'dst': dst_ip
                                }
                                cntr += 1
                                print "adding flow"
                                assignFlow(iface,
                                           prio[dst_ip]['id'],
                                           prio[dst_ip]['dst'])
            for k in prio.keys():
                if prio[k]['lease'] > 0:
                    prio[k]['lease'] -= 1
                    if prio[k]['lease'] < 0:
                        print "removing flow"
                        removeFlow(iface,prio[k]['id'])
                        del prio[k]
                    # else:
                    #     s.close() 
                    #     input.remove(s) 
    except KeyboardInterrupt, e:
        None
        
def main():
    global options
    
    nm_listen(options.iface)
    
    return 0;

if __name__ == '__main__':
    try:
        parser = optparse.OptionParser(
            formatter=optparse.TitledHelpFormatter(),
            usage=globals()['__doc__'],
            version='')
        
        parser.add_option('-v', '--verbose', action='store_true',
                          default=False, help='verbose output')

        parser.add_option('-l', '--lease', action='store',
                          default=-1,
                          help='Flow priority lease time (default:inf)')

        parser.add_option('-r', '--rate', action='store',
                          default=1e2,
                          help='Total bandwidth allocation (default:100mb)')

        parser.add_option('-H', '--high', action='store',
                          default=.8,
                          help='%% of link given to prio flows (default:80%)')

        parser.add_option('-i', '--iface', action='store',
                          default='eth2', 
                          help='Interface to apply rules')


                        
        (options, args) = parser.parse_args(args=sys.argv)
        start = dt.now()
        sys.exit(main())

    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        print 'Unexpected exception "%s"' % str(e)
        traceback.print_exc()
        sys.exit(1)
