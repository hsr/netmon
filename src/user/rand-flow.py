#!/usr/bin/python

#!/usr/bin/env python
 
"""
SYNOPSIS
 
    Usage: ./rand-flow.py [options]
 
DESCRIPTION
 
    Runs multiple TCP network flows using Netperf.
    Each flow is started in a thread and they might or might not be dependent.
    The sizes are 
    
"""

import subprocess
import optparse
import traceback
import sys
import time
import numpy as np
from threading import Thread
from datetime import datetime as dt
import sys
import subprocess
from threading import Thread

def abort(msg):
    print msg
    sys.stdout.flush()
    sys.exit(1)

def warn(msg):
    print "WARNING:", msg
    sys.stdout.flush()
    
def run(cmd, bg=False, checkReturn=True, verbose=False):
    if verbose:
        print "cmd:", cmd
        sys.stdout.flush()
    try:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, 
                             stderr=subprocess.PIPE, shell=True,
                             executable='/bin/bash')
        if bg:
            return
        output,stderr = p.communicate()
        if checkReturn and p.returncode:
            abort('unexpected return code %d for "%s".\n%s\n%s' % 
                  (p.returncode, cmd, output, stderr))        
        return output.strip()
    except Exception, e:
        None
        

def ssh(machine, cmd, user = 'root', bg=True, checkReturn=True, verbose=False):
    return run("ssh -o ConnectTimeout=3 %s@%s '%s'" % (user, machine, cmd), 
               bg=bg, checkReturn=checkReturn, verbose=verbose);

# Run the cmd given on each of the machines in parallel. 
# Note: This function never runs ssh in background.
#       This function will block until all the sshs return
def pssh(machines, cmd, user = 'root', checkReturn=True, verbose=False):
    threads = [];

    for m in machines:
        threads += [Thread(target=ssh,
                           args=([m,cmd,user,False,checkReturn,verbose]))]

    # TODO: Threads have a sys return code, check it!
    map(lambda t: t.start(), threads)
    try:
        map(lambda t: t.join(60), threads) # It shouldn't take more than 1 min
    except KeyboardInterrupt: # Ctrl-C
        print "Thread wait interrupted"
    except EnvironmentError:
        print "Signal received/Env error"
    except:
        print "An error occurred!"
    sys.stdout.flush()

    for t in threads:
        if t.isAlive():
            warn("pssh: a thread didn't finish!")
            return 0
    return -1

def scp(machine, localfile, remotepath, user = 'root'):
    return run("scp -o ConnectTimeout=3 %s %s@%s:%s" % \
                   (localfile, user, machine, remotepath))




randSeed = 100;
start=0;

servers = ["dcswitch%d" % d for d in range(119,122)]

import socket
hostname = socket.gethostname()

def rand_netperf(thread_id, avg_size=100, avg_idle=0, transfers=10):
    global randSeed, servers, options;
    np.random.seed(randSeed+thread_id);
    
    if int(options.transfers) != 10:
        transfers = int(options.transfers)

    if int(options.size) != 10:
        avg_size = int(options.size)
    
    if options.verbose:
        print 'thread %d running %d transfers' % (thread_id,transfers)
    
    targets = np.random.random_integers(0, len(servers)-1, size=transfers)    
    transfs = np.random.poisson(avg_size, transfers);
    
    for i in range(len(targets)):
        size   = transfs[i]*1024*1024; #transfer size in MB
        target = servers[targets[i]]; #target host for this transfer
        if hostname in target or target in hostname:
            continue
        #print "netperf -H %s -l -%d -t TCP_STREAM" % (target,size)
        if options.verbose:
            print "trying %s" % target
        ssh(target, 'iperf -s -p 5252', checkReturn=False, bg=False)
        if options.verbose:
            print "iperf -c %s -n %d -p 5252" % (target,size)
        run("iperf -c %s -n %d -p 5252" % (target,size))

def main():
    global options, start, servers
    
    options.threads = int(options.threads)
    
    if len(str(options.servers)):
        servers = str(options.servers).split(',')

    try: 
        threads = []
        while options.threads > 0:
            threads += [Thread(target=rand_netperf,args=([len(threads),options.size,options.transfers]))]
            options.threads -= 1;
            threads[-1].start();
        for t in threads:
            t.join();
        
    except KeyboardInterrupt, e: # Ctrl-C
        print "Ctrl+C"
        sys.exit(1)
    return 0;

if __name__ == '__main__':
    try:
        parser = optparse.OptionParser(
            formatter=optparse.TitledHelpFormatter(),
            usage=globals()['__doc__'],
            version='')
        
        parser.add_option('-v', '--verbose', action='store_true',
                          default=False, help='verbose output')

        parser.add_option('-t', '--threads', action='store',
                          default=1, 
                          help='How many threads should run (default 1)')

        parser.add_option('-S', '--servers', action='store',
                          default='', 
                          help='Comma-separated list of servers')

        parser.add_option('-s', '--size', action='store',
                          default=100, 
                          help='Average flow size in megabytes (default 100MB)')

        parser.add_option('-T', '--transfers', action='store',
                          default=10, 
                          help='# of transfer per thread (default 10)')

                        
        (options, args) = parser.parse_args(args=sys.argv)
        start = dt.now()
        sys.exit(main())

    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        print 'Unexpected exception "%s"' % str(e)
        traceback.print_exc()
        sys.exit(1)