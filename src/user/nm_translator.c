#include <stdio.h>

#include <linux/if_ether.h>   // ETH_P_ARP = 0x0806, ETH_P_ALL = 0x0003
#include <sys/socket.h>
#include <netpacket/packet.h>
#include <net/ethernet.h> /* the L2 protocols */

#include "nm_translator.h"
#include "net_util.h"

static struct sockaddr_ll saddr;

const u8 broadcast_mac[6] = {0xff,0xff,0xff,0xff,0xff,0xff};

struct host_db {
	uchar mac[6];
	u32   id;
	struct UT_hash_handle hh;
};

static inline unsigned compare_ether_addr(const u8 *addr1, const u8 *addr2){
        const u16 *a = (const u16 *) addr1;
        const u16 *b = (const u16 *) addr2;

        return ((a[0] ^ b[0]) | (a[1] ^ b[1]) | (a[2] ^ b[2])) != 0;
}

static int
nm_translator_listen(int sock, uchar *client_mac) {
	uchar frame[ETH_FRAME_LEN];
	struct ethhdr *eth;
	int   rcv = 0, exp_rcv = 
		sizeof(struct ethhdr);

	printf("listening\n");
	if ((rcv = recv(sock, frame, ETH_FRAME_LEN, 0)) < exp_rcv)
		return -1;

	eth = (struct ethhdr *) &frame;
	if (eth->h_proto == ETH_P_NETMON_Q) {
		/* Queries are broadcasted, replies are directed */
		if (!compare_ether_addr(eth->h_dest, broadcast_mac)) {
			memcpy(client_mac, eth->h_source, 6);
		}
	}
	return 0;
}

void
print_hash(struct host_db *hdb) {
	struct host_db *h, *tmp;
	HASH_ITER(hh, hdb, h, tmp) {
		printf("hash:"ETH_ADDR_FMT" = %d\n", ETH_ADDR_ARGS(h->mac), h->id);
	}
}

void
nm_fill_saddr(int ifindex) {
	memset(&saddr, 0, sizeof(saddr));

	/*RAW communication*/
	saddr.sll_family   = PF_PACKET;	
	saddr.sll_ifindex  = ifindex;
	saddr.sll_hatype   = ETH_P_NETMON_Q;
	saddr.sll_pkttype  = PACKET_OTHERHOST;
	saddr.sll_halen    = ETH_ALEN;		

	/* memcpy((void*)buffer, (void*)dest_mac, ETH_ALEN); */
	/* memcpy((void*)(buffer+ETH_ALEN), (void*)src_mac, ETH_ALEN); */
	/* eh->h_proto = 0x00; */

}

/* Sends a frame with h_proto ETH_P_NETMON_Q to the destination pointed by dst
*/

static int
nm_translator_reply(int sock, uchar *client_mac, u32 id) {
	uchar frame[ETH_FRAME_LEN];
	uchar local_mac[6] = { 0,0,0,0,0,0 }; /* TODO */
	struct ethhdr *eth;
	struct nm_query_msg *nmq;

	/* eth frame */
	eth = (struct ethhdr *) &frame;
	memcpy(eth->h_dest,   client_mac, 6);
	memcpy(eth->h_source, local_mac,  6);
	eth->h_proto = htons(ETH_P_NETMON_Q);

	/* nm_query_msg */
	nmq = (struct nm_query_msg*)(eth+1);
	nmq->id = htonl(id);

	memcpy (saddr.sll_addr, client_mac, 6);

    /*send the packet*/
	if (sendto(sock, frame, sizeof(struct ethhdr) + sizeof(struct nm_query_msg),
			   0, (struct sockaddr*)&saddr, sizeof(saddr)) < 0)
		return -1;
	return 0;
}

static int
nm_sock_open() {
	int s;
	if ((s = socket(PF_INET, SOCK_RAW, htons(ETH_P_ALL))) < 0)
		return -1;
	setsockopt(s, SOL_SOCKET, SO_BINDTODEVICE, "eth2", 4);
	return s;
}

void *
nm_translator(void *iface) {
	struct host_db *hdb = NULL;
	struct host_db *h = NULL;
	uchar mac[6];
	int ifindex;
	int sock;

	if ((ifindex = nu_get_ifindex((char*)iface)) < 0)
		eexit("could not retrieve ifindex for given interface");

	if ((sock = nm_sock_open()) < 0)
		eexit("could not open socket for translator");

	/* TODO: figure out how to get address */
	nm_fill_saddr(ifindex);
	
	for (;;) {
		printf("waiting for request\n");
		/* Wait for request */
		if (nm_translator_listen(sock, mac) < 0)
			eexit("error receiving db request");

		/* Lookup mac address */
		HASH_FIND(hh, hdb, &mac, sizeof(mac), h);
		if (!h) {
			/* Lookup if not found, create it */
			h = (struct host_db *) malloc (sizeof(struct host_db*));
			if (!h)
				eexit("could not alloc host_db");
			memcpy(h->mac, mac, 6);
			h->id = HASH_COUNT(hdb) + 1;
			HASH_ADD(hh, hdb, mac, sizeof(mac), h);
		}
		print_hash(hdb);
		nm_translator_reply(sock, mac, h->id);
	}
}
