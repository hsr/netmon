#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#include <netinet/ip.h>
#include <net/if.h>
#include <net/ethernet.h>
#include <linux/if_ether.h>   // ETH_P_ARP = 0x0806, ETH_P_ALL = 0x0003

#include "util.h"
#include "net_util.h"

/**
 * Given an network interface name, write the mac address associated with the
 * interface in the *mac pointer given as parameters.
 * Returns 0 on success, -1 on error
 **/
int
nu_get_mac (char *ifname, uchar *mac) {
    struct ifreq buffer;
    int s;

    s = socket(PF_INET, SOCK_DGRAM, 0);
    memset(&buffer, 0x00, sizeof(buffer));
    strcpy(buffer.ifr_name, ifname);
    if (ioctl(s, SIOCGIFHWADDR, &buffer) < 0) {
		close(s);
		return -1;
	}

    for( s = 0; s < 6; s++ )
    	printf("%.2X ", (unsigned char)buffer.ifr_hwaddr.sa_data[s]);

	memcpy(mac,buffer.ifr_hwaddr.sa_data, 6);
	return 0;
}

/**
 * Given an network interface name, returns the ifindex of the interface.
 * Returns -1 on error, the interface number on success.
 **/
int
nu_get_ifindex (char *ifname) {
    struct ifreq buffer;
    int s;

    s = socket(PF_INET, SOCK_DGRAM, 0);
    memset(&buffer, 0x00, sizeof(buffer));
    strcpy(buffer.ifr_name, ifname);
    if (ioctl(s, SIOCGIFINDEX, &buffer) < 0) {
		close(s);
		return -1;
	}

	return buffer.ifr_ifindex;
}

inline int
nu_is_ip(u16 eth_type) {
	return (ntohs(eth_type) == 0x0800);
}

inline int
nu_frame_is_ip(u8 *data) {
	return nu_frame_type(data, 0x0800);
}

inline int nu_frame_type(u8 *data, u16 cmp_type) {
	u16 eth_type = (u16) data[12];
	return (ntohs(eth_type) == cmp_type);
}

inline void
nu_print_eth(u8 *data) {
#ifdef __KERNEL__
	printk(KERN_INFO "pkt from "ETH_ADDR_FMT" -> "ETH_ADDR_FMT,
		ETH_ADDR_ARGS(&data[6]),ETH_ADDR_ARGS(&data[0]));
#else
	printf("pkt from "ETH_ADDR_FMT" -> "ETH_ADDR_FMT"\n",
		   ETH_ADDR_ARGS(&data[6]),ETH_ADDR_ARGS(&data[0]));
#endif
}
